---
description: Switchroot (Android and Linux) issues and solution page.
---

# Common Issues

## Common Issues <a href="#common-issues" id="common-issues"></a>

**DISCLAIMER: Switchroot will (and can) only provide support for our official builds which you can find** [**here**](https://download.switchroot.org/)**.**

## Android <a href="#android" id="android"></a>

{% tabs %}
{% tab title="Boot" %}
<details>

<summary>Can't Flash Zips in TWRP</summary>

Be sure to get latest version of the zip you're trying to flash or make sure you are using the latest OTA (currently 04/08/2022)

</details>

<details>

<summary>Not booting</summary>

Ensure your SD fat32 partition matches the layout indicated by our [guide](android/android-10/10-q-setup-guide.md#steps)\
Also make sure you read the install guide FULLY and did not skip any steps or substitute in older versions of zips or MindTheGApps/OpenGApps.

</details>
{% endtab %}

{% tab title="Controls" %}
<details>

<summary>Motion Controls</summary>

If you have problems with autorotation, or motion controls in games, ensure that you have updated to the latest OTA.\
If you still feel like you have gyro drift, you can attempt to manually calibrate your gyro:\
\
Instructions for gyro.txt\
1\. Use a gyro tester app like Physics Toolbox\
2\. Find the average distance from zero for each axis (x, y, z)\
3\. Create switchroot/android/gyro.txt of the following form:\
x, y, z\
1.0, 1.0, 1.0\
(Yes, write 1 as 1.0 in the file)

</details>

<details>

<summary>Joycons/ProCons</summary>

If your joycons do not autopair in android, make sure that you have paired them in HorizonOS and dumped them in Hekate/NYX. Pro Controllers do not have autopair function. If you wish to use them via bluetooth in android, you will have to pair them manually in the bluetooth settings (reset whenever you use the controller with other OSs).

</details>
{% endtab %}

{% tab title="WIFI" %}
<details>

<summary>Unable to connect to wifi.</summary>

If you are unable to connect to your home WIFI network and have access to your WIFI router settings, try connecting via your 2.4GHz network or changing your 5Ghz channel to 40 or below.

</details>

<details>

<summary>Need to update offline.</summary>

If you don’t have wifi and can’t make it work, download the latest icosa-tab zip, unzip it, and put the lineage zip inside it on the root of your sd. Then flash it in twrp.

</details>
{% endtab %}

{% tab title="Bluetooth" %}
JoyCon/ProCon specific issues covered under the Controls tab. There are no other known common issues. Ask us in the Switchroot Discord Server if you are having problems.
{% endtab %}

{% tab title="Applications" %}
Issue: My "insert application name here" does not show in the playstore or will not launch Answer: Certain app makers BLOCK devices or maintain a whitelist of allowed devices which can run their app. You can attempt to use one of the multiple device spoofing apps to see if spoofing your devices with another (such as the google pixel) allows you to run your desired application.
{% endtab %}
{% endtabs %}

## Linux <a href="#linux" id="linux"></a>

{% tabs %}
{% tab title="WIFI/Bluetooth" %}
Issue: WIFI or bluetooth stopped working after using my system when it was working before. Answer: Update to the L4T 5.0.0+. Ubuntu overwrites the Switch specific WIFI firmware on `apt upgrade`, this has been fixed in 3.4.2 and later.
{% endtab %}

{% tab title="Applications" %}
<details>

<summary>I tried installing an app and it doesn't run</summary>

If you installed an app following a tutorial, or from a software makers website, its likely that it is an x86\_64/amd64 application. The switch is an arm64 device and can NOT run these applications natively. Look for a native arm64 version of your app or alternatives.

</details>

<details>

<summary>My Snap or Flatpak package does not run well</summary>

Snaps do not have GPU hardware acceleration on L4T Linux due to nvidia propretary tegra drivers.\
\
Flatpaks have the same issue UNLESS you:\
A: Are using Ubuntu Jammy or Fedora which have Flatpak hardware acceleration out of the box\
B: Are using Ubuntu Bionic and use theofficialgman's flatpak tegra BSP by running anything in the L4T-Megascript or from the pins in #ubuntu.\
\
In most cases, you should prefer building your desired packages from source code, or using available deb or PPAs.

</details>
{% endtab %}

{% tab title="Stutter/FramePacing Issue when Handheld" %}
Nvidia Tegra and Desktop Drivers have FramePacing/Stutter issues when the display is Rotated. The switch's internal display is actually a Portrait display which is rotated to Landscape by the OS.\
This is an upstream driver bug that, to this day, is unfixed on Nvidia Xorg Drivers.\
\
Originally tracked via this issue on nvidia forums: [https://forums.developer.nvidia.com/t/very-poor-framepacing-and-performance-when-using-xorg-rotation/178451](https://forums.developer.nvidia.com/t/very-poor-framepacing-and-performance-when-using-xorg-rotation/178451)\
Now tracking under the new issue: [https://forums.developer.nvidia.com/t/request-to-re-openvery-poor-framepacing-and-performance-when-using-xorg-rotation-178451-due-to-not-being-fixed-in-32-7-1/205322](https://forums.developer.nvidia.com/t/request-to-re-openvery-poor-framepacing-and-performance-when-using-xorg-rotation-178451-due-to-not-being-fixed-in-32-7-1/205322)
{% endtab %}
{% endtabs %}

## Hardware <a href="#hardware" id="hardware"></a>

{% tabs %}
{% tab title="Display" %}
<details>

<summary>Touchscreen</summary>

If you encounter an issue with your touch inputs not being recognized / registered in our projects, here is a list of step to guide you in determining the issue

1. Verify your whole touchscreen is working in HorizonOS. Use the touchscreen calibration/tester in the settings to verify.
2. Use the calibration tool in hekate
3. If your whole touchscreen is not working in HorizonOS, you may need to re-seat your switch touchscreen connector.

In case you touch inputs are still not getting recognized, take a screenshot of your display informations in Hekate as well as a log from the OS you're using and send it in our discord

</details>

<details>

<summary>Internal Display</summary>

No known common issues. Ask us in the Switchroot Discord Server if you are having problems.

</details>

<details>

<summary>External Display Corruption</summary>

If you see corruption on screen when docking: Undock and re-dock the switch, sometimes the HDMI/DP handhake process does not work correctly. This happens more frequently when using 4K display output.

</details>

<details>

<summary>External Display No Video</summary>

This can be caused by one of a few issues:

1. If you are using an Original Nintendo Switch Dock, and you get no video output in Android/Linux and your switch display does NOT turn off, then you have an HDMI Hotplug issue. Try a different TV/monitor, or a branded/the official HDMI cable.

Why this happens: HorizonOS does NOT require HDMI hotplug to output via HDMI. Try it yourself, if you dock with an hdmi cable disconnected in HOS, the switch display will still turn off. Android/Linux require this feature (to make them compabile with hubs/docks so users can still use the internal display when HDMI is not plugged in).

2. If you are using an Original Nintendo Switch Dock, and you get no video output in Android/Linux and your switch display DOES turn off, then you likely have an HDMI handshake issue. Try a different TV/monitor, or a branded/the official HDMI cable. If your TV is 4K, try a 1080p or lower resolution one.
3. If you use 3rd party Dock/Hub does not produce video output but the official dock works: If your dock does not work in L4T Ubuntu 5.0.0, let us know in the Discord.

</details>
{% endtab %}
{% endtabs %}
