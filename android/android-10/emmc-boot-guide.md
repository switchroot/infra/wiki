# 10 eMMC Boot Guide

### Prerequisite <a href="#pre-requisite" id="pre-requisite"></a>

Backup the eMMC with boot0/1 and rawnand.bin\
Have a PC running linux for eMMC preparation.\
Download the eMMC overlay from [the latest artifacts zip](https://gitlab.com/switchroot/kernel/l4t-device-tree-overlays/-/pipelines/707454658/builds)

### Installation <a href="#installation" id="installation"></a>

#### eMMC preparation (for Android and Linux alongside HOS on a larger eMMC) <a href="#emmc-preparation-for-android-and-linux-alongside-hos-on-a-larger-emmc" id="emmc-preparation-for-android-and-linux-alongside-hos-on-a-larger-emmc"></a>

1. If you have a new eMMC, install it and write back boot0/1 and the rawnand bin in hekate.
2. Under Linux mount the eMMC with hekate. Deactivate write protect!
3. Check where linux has mounted the eMMC (sdb, sdc ...)
4. Open `gparted` and confirm the resize from the eMMC gpt table.
5. Close gparted.
6. Open Terminal and `sudo gdisk /dev/sdx` (where x is your the device letter)
7. Press x=>s=>20=>v=>w=>y to enlarge the gpt table to 20 entries. (If you also want to do linux on the eMMC you have to enter 24.)
8. Open gparted again.
9. HOS has 11 partitions, Android need 9 partitions we have to create them.
10. Add the 9 partitions and set the file system as deleted.

| Partition             | Name   | Size    |
| --------------------- | ------ | ------- |
| Vendor                | vendor | 1GiB    |
| System                | APP    | 2GiB    |
| Linux Kernel          | LNX    | 32MiB   |
| Recovery (TWRP)       | SOS    | 64MiB   |
| Device Tree Reference | DTB    | 1MiB    |
| Encryption\*          | MDA    | 16MiB   |
| Cache                 | CAC    | 700 MiB |
| Miscellaneous         | MSC    | 3MiB    |
| Userdata              | UDA    | Custom  |

11. If you want to add Linux, add a partition with the label of your choice (e.g.: `SWR-UBU` for Ubuntu Bionic) and 3 dummy partitions with 1mb unformated. In order to have 24 partitions. gdisk cannot create 21 partitions because the sector is filled automatically (multiple of 4) if there are only 21 partitions on the eMMC and not 24 Horizon does not start. The bootloader checks that and Horizon brings up a pink screen.

#### Flashing the eMMC <a href="#flashing-the-emmc" id="flashing-the-emmc"></a>

1. Using `dd` in terminal flash the following (replacing `hekate`'s `Flash Android` step)

**ANDROID 10**

| File                 | Label |
| -------------------- | ----- |
| `boot.img`           | LNX   |
| `twrp.img`           | SOS   |
| `tegra210-icosa.dtb` | DTB   |

2. For Linux, merge the install image with `cat l4t.0* > ubuntu.img` and `dd` it to SWR-UBU (replacing `hekate`'s `Flash Linux` step). Refer to [step 3 and 4 here](../../linux/linux-usb-or-emmc-boot.md#emmc-boot) for more detail.
3. Add the release files (bootfiles, ini, lineage zip, any extra zips)
4. If running Android 10, add `overlays=tegra210-icosa_emmc-overlay` to a new file, add `emmc=1` to `switchroot/android/uenv.txt`, and add the emmc overlay from [build artifacts](https://gitlab.com/switchroot/kernel/l4t-device-tree-overlays/-/pipelines) to `switchroot/overlays`
5. Boot `hekate` -> More Configs -> click on the entry for Switchroot Android 10 while you press VOL +
6. In recovery, flash the lineage zip and any extra zips

**WARNING:** When setting up Android, under no circumstances use the sd card as a memory extension (adopted storage), otherwise it will be formatted. The FAT32 partition will be mounted regardless, so no need.
