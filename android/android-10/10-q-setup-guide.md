---
description: >-
  Installation guide/tutorial for Switchroot Android 10 on Nintendo Switch,
  based on the original guide at
  https://forum.xda-developers.com/t/rom-unofficial-switchroot-android-10.4229761/
---

# 10 Setup Guide

This guide covers installation of the public release of Switchroot Android 10/Q, based on LineageOS 17.1, on the Switch. The original guide upon which this is based can be found [here](https://forum.xda-developers.com/t/rom-unofficial-switchroot-android-10.4229761/). All photos are credited to @bylaws on XDA Developers Forum.

Switchroot Android Q is only compatible with unpatched v1 Switches (generally units produced before late 2018) or "ipatched" v1 Switches with a hardmod.

### Requirements

* RCM-exploitable/unpatched or hardmodded ipatched v1 Switch ([CHECK HERE FIRST](https://ismyswitchpatched.com/))
* (For unpatched) RCM exploitation method (Usually a jig, but other methods can be found [HERE](https://noirscape.github.io/RCM-Guide/))
* Computer
* USB-C cable/USB-C Switch autoinjector
* High quality SD card >= 16GB
* microSD card reader for the computer

### Prerequisites

* Nuke any other Switchroot Android builds before attempting to install this release
* Ensure SD is formatted as FAT32

### Steps

1.  Download `hekate` and the latest Androioid 10 release

    a. Download the latest version of the `hekate` bootloader from [HERE](https://github.com/CTCaer/hekate/releases) (`hekate_ctcaer_[version]_Nyx_[version].zip`)

    b. Unzip the `hekate` archive to the root of the SD card

    c. Download the latest zip [HERE](https://download.switchroot.org/android-10/) of either `icosa-tab` (standard tablet Android) or `icosa-atv` (Android TV)

Your SD card should now look like this, assuming it is shared with HOS (Horizon OS/Switch OS):

```
root
|- bootloader
|  |- ini
|  |  |- ...
|  |- payloads
|  |  |- ...
|  |- res
|  |  |- ...
|  |- sys
|  |  |- ...
|- Nintendo
|  |- ...
```

2.  Prepare Switch, boot into hekate, and dump JoyCon pairing info\
    a. Boot into HOS and ensure Joy-Cons are paired

    b. Insert your SD card into your Switch, slide the RCM jig into the right rail

    c. Slide the RCM jig into the right rail, and hook your Switch up to your PC (or other payload injector) via USB-C and power off the console\
    d. Press Power and VOL + simultaneously to enter RCM mode and inject the hekate payload (`hekate_ctcaer_[version].bin`) that came with your downloaded hekate zip\
    NOTE: To keep it simple you can use [this](https://switch.exploit.fortheusers.org/) in Google Chrome (not compatible with other browsers), check "Upload .bin payload" and select the payload mentioned above, then click "Do the thing!" and select "APX" from the pop-up menu.\
    e. Once booted into `hekate`, you can remove the RCM jig, and launch Nyx Settings

<figure><img src="../../.gitbook/assets/a11-1.jpg" alt=""><figcaption></figcaption></figure>

f. Dump your Joy-Con BT pairing information (with your Joy-Cons railed)

<figure><img src="../../.gitbook/assets/a11-2.jpg" alt=""><figcaption></figcaption></figure>

g. Ensure the message is correct, otherwise physically re-connect and re-pair the JoyCons in HOS and try again

<figure><img src="../../.gitbook/assets/a10-3.png" alt=""><figcaption></figcaption></figure>

3. Partition SD card in hekate\
   a. Navigate to `hekate`'s partition manager (on the Tools tab)

<figure><img src="../../.gitbook/assets/a11-4.jpg" alt=""><figcaption></figcaption></figure>

b. Use sliders to select appropriate space for each OS (we recommend at least 16 GB for Android, although it should be usable with \~8GB), then press Next Step\
NOTE: this step creates multiple partitions for a somewhat standard AOSP layout -- look [HERE](../../documentation/partition-docs.md) for more information if interested\
c. You may want backup Nintendo folder or any CFW files if necessary via `hekate`'s SD UMS tool over USB-C or by removing the SD card and reading directly on a PC (`hekate`/Nyx can pause and reload when SD is removed), but `hekate` will offer to backup and restore up to 1 GB of data from your existing file-system.

4. Add ROM zips to SD\
   a. Plug the Switch into your PC and select "SD UMS"\
   b. Extract the `icosa-tab` or `icosa-atv` `.zip` to the root of the SD card

Your SD should now look like this:

```
root
|- bootloader
|  |- ini
|  |  |- ...
|  |- payloads
|  |  |- ...
|  |- res
|  |  |- ...
|  |- sys
|  |  |- ...
|- Nintendo
|  |- ...
|- switchroot
|  |- android
|  |  |- ...
|  |- install
|  |  |- ...
|- lineage-17.1-[date]-UNOFFICIAL-[device]-signed.zip
|- <any other zips you added>
```

5. Add other zips and enter recovery\
   a. Check out the [Extra Zips](10-q-setup-guide.md#extra-zips) section before continuing and add GApps for Google Play if you want it to the root of the SD card\
   b. If you have any other flashable zips, place them on the root of the SD card\
   c. Safely remove/eject the SD UMS device from your computer\
   d. Back on the Switch, select "Flash Android" and accept the prompt to reboot to recovery.\
   e. Press Install, then "Select Storage" in TWRP and select "Micro SD Card"\
   f. Install the Lineage zip, followed by GApps and any other zips you may have added\
   g. Upon installing the final zip, wipe cache/dalvik and reboot to system.
6. First boot\
   a. Your JoyCons may not auto-pair on first boot--reboot to auto-pair your dumped JoyCons in Android
7. Subsequent boots\
   a. Boot Lineage 17.1 from More Configs --> Switchroot Android 10 in `hekate`\
   b. Hold VOL + on choosing this option to get back to recovery to flash zips\
   c. Hold VOL - on choosing this option to get back to `hekate` without having to reinject a payload\
   d. Rebooting the Switch will take you back to stock firmware
8. Asking for support/If you get stuck\
   a. First, get a screenshot of SD Info in `hekate`--this will allow us to rule out the most common issue first\
   b. If you are experiencing seemingly unintended behavior while already booted into Android, try and get us a log--find a guide on how to install `adb`, enable USB debugging in developer options on the Switch, plug into the computer with adb installed, and run `adb logcat > switchroot.log`. If this seems like too much for you, just get a bug report from developer settings.\
   c. Submit the screenshot and logs if needed to in our Discord server--the invite link can be found [HERE](https://discord.gg/N9PPYXjWMY).

### Extra Zips

Flashable zips are system additions that can be added from TWRP. Hold Vol+ on (re)boot to enter recovery and flash the desired zip. DO NOT flash incompatible or random zips.

* GApps--We use upstream LineageOS recommendations for Google apps. For Android 10 this is [MindTheGapps for tab](https://github.com/MindTheGapps/10.0.0-arm64/releases/latest) and [OpenGApps for ATV](https://opengapps.org/?api=10.0\&variant=tvstock)
  * These use a minimal setup to fit better and add less bloatware
  * Don't flash ATV packages on tab or vice-versa
* Super Overclock--Grab the latest version from [HERE](https://github.com/Lumince/SwitchRoot-Q-Overclock) to overclock your Switch CPU to 2091 MHz
  * This can cause system instability and reduce hw lifespan, but can get some emulated titles over the "playable" hump
  * More granular OC control is available in newer Switchroot Android releases
* JoyCon Xbox Layout--Grab the package from [HERE](https://download.switchroot.org/android-10/extras/joycon-xbox.zip) to use an Xbox controller-style control scheme
  * Do NOT flash this on newer Switchroot Android versions!
* Magisk--The .apk can be renamed to .zip and flashed in TWRP for a [root ](https://en.wikipedia.org/wiki/Rooting_\(Android\))solution
  * Issues caused by Magisk are not our fault and will not be fixed (by us)--please do not file bug reports relating to kernel or system stability after installing Magisk or other root patching solutions

### Updating

Updates are pushed through OTA (over-the-air) updates, just like any Android OEM's. Use the Updater in Settings to get the latest updates without losing data. Note that the `20220408` version is the latest and last Switchroot Android 10 release, and newer versions cannot be updated to OTA--see release pages for details.

### Maintainers

bylaws

makinbacon - [https://paypal.me/makinbacon21](https://paypal.me/makinbacon21)
