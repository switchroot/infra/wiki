---
description: DEPRECATED
---

# Android 10

Switchroot Android 10 (Q) is a downstream LineageOS 17.1 release supporting the `odin` SKU (v1)

Kernel: Downstream Switchroot L4T kernel, rebased on r32.4.4 kernel-4.9, kernel-nvidia, kernel-nvgpu

Blobs: Shield Experience 8.2.2

### Installing

To install official release or install, please follow our official guide: [10-q-setup-guide.md](10-q-setup-guide.md "mention")

For an eMMC install (supported but not official), follow our [emmc-boot-guide.md](emmc-boot-guide.md "mention")

### Customization

To customize, use ZIPs from our [official download site](https://download.switchroot.org/android-10/extras/) or from Android 10 targeted 3rd parties (e.g. [https://github.com/Lumince/Switchroot-Q-OC](https://github.com/Lumince/Switchroot-Q-OC))
