---
description: DEPRECATED
---

# Android 8.1

Switchroot Android 8.1 (O) is a downstream LineageOS 15.1 release supporting the `odin` SKU (v1)

Kernel: Downstream LineageOS L4T 3.10 kernel, based on rel-29-3.10-shield

Blobs: Shield Experience 7.2.3 (?)

### Installing

We do NOT recommend installing this. 8.1 was mostly a cobbled together proof-of-concept. If you are testing something, use [https://download.switchroot.org/android-10/archived/](https://download.switchroot.org/android-10/archived/) for prebuilt SD card image downloads.

### Customization

To customize, use ZIPs from our [official download site](https://download.switchroot.org/android-10/archived/extras/) or from Android 8.1 targeted 3rd parties (unaware of any currently maintained)
