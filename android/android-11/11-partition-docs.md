# 11 Partition Docs

Android 11 utilizes a legacy, or [System-as-Root partition](https://source.android.com/docs/core/architecture/partitions/system-as-root#about-system-as-root) scheme enabling easy system partition loading from initramfs. This was the default from \~Android 9-11.

Required partitions and sizes (order should not matter but it is best to keep in order for semantics, and the GPT table must come after the MBR table):

<table><thead><tr><th width="233">Partition</th><th>Name</th><th>Size</th></tr></thead><tbody><tr><td>Vendor</td><td>vendor</td><td>1GiB</td></tr><tr><td>System</td><td>APP</td><td>2GiB*</td></tr><tr><td>Linux Kernel</td><td>LNX</td><td>32MiB</td></tr><tr><td>Recovery (TWRP)</td><td>SOS</td><td>64MiB</td></tr><tr><td>Device Tree Reference</td><td>DTB</td><td>1MiB</td></tr><tr><td>Metadata**</td><td>MDA</td><td>16MiB</td></tr><tr><td>Cache</td><td>CAC</td><td>700 MiB</td></tr><tr><td>Miscellaneous</td><td>MSC</td><td>3MiB</td></tr><tr><td>Userdata</td><td>UDA</td><td>Custom***</td></tr></tbody></table>

( 1 GiB = 1024 MiB )

\* `hekate` now creates 3 GiB, but public images only support 2 GiB systemimage--compile 10 or 11 manually to support&#x20;

\*\* You may encounter errors in Android if your encryption (MDA) partition is not properly zeroed before use. Be sure to zero it when altering the partition. Encryption is not recommended and may not function properly.

\*\*\* If making Android partitions by hand, take into account that if you have no emuMMC partition (which would go after the Android userdata partition) you should reserve 1MB of unallocated space at the end of the partition table.

Data is based off [`hekate`'s partitioner source code](https://github.com/CTCaer/hekate/blob/master/nyx/nyx_gui/frontend/gui_tools_partition_manager.c).
