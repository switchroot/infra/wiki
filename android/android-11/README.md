---
description: DEPRECATED
---

# Android 11

Switchroot Android 11 (R) is a downstream LineageOS 18.1 release supporting all SKUs (`odin`/v1, `modin`/v2, `vali`/Lite, `fric`/OLED)

Kernel: Downstream Switchroot L4T kernel, rebased on rel-shield-r kernel-4.9 and r34 kernel-nvidia, kernel-nvgpu

Blobs: Shield Experience 9.1.1

### Installing

To install official release or install, please follow our official guide: [11-r-setup-guide.md](11-r-setup-guide.md "mention")

For an eMMC install (supported but not official), follow our [11-emmc-guide.md](11-emmc-guide.md "mention")

### Customization

To customize, see the [11-r-ini-guide.md](11-r-ini-guide.md "mention")--note that enabling DDR200 can cause issues for unsupported cards, but is enabled by default on all Samsung.
