---
description: >-
  A helpful guide covering updating to and clean installing Switchroot Android
  11
---

# 11 Setup Guide

This guide covers installation of the public beta release of Switchroot Android 11/R, based on LineageOS 18.1, on all Switch models.

### Disclaimer

This is BETA SOFTWARE, delivered to you AS-IS with NO WARRANTY. While hardware damage is unlikely outside of heavy overclocking, you may run into issues, etc.. However, this is a rather stable beta release, and extensive testing has shown it to work just fine, minus a couple remaining missing Q features (see the latest release highlights in the Discord announcement).

### Updating from Q

Updating is pretty easy! Download the latest zip, extract the `lineage-18.1....zip` file, and drop it on the root of your SD card. Boot into TWRP (VOL+ while booting Android) and flash the Lineage zip, then just reboot right into Android. Your releavant ini configuration and any uenv will be ported over automatically. You may experience a crash/forced reboot to recovery shortly after first boot--just ignore this and reboot without selecting Format Data. If you encounter long-term instability, just back up relevant stuff with Seedvault or standard Google account backup, wipe everything in recovery, and hard reinstall.

### Requirements

* RCM-exploitable ([CHECK HERE](https://ismyswitchpatched.com/)) or hard-modded Switch
  * RCM exploit requires an exploitation method (Usually a jig, but other methods can be found [HERE](https://noirscape.github.io/RCM-Guide/))
  * Switchroot does not endorse and is not affiliated with modchip manufacturing, development, etc., and for modchip support and recommendations you must go elsewhere
* Computer
* High quality microSD card >= 16GB
  * SD card quality directly affects Switchroot Android performance
  * See our [SD card guide](../../sd-card-guide.md) for details
* microSD card reader for the computer or USB-C cable for hekate UMS

### Prerequisites

* Nuke any Switchroot Android O/8.1 installations or dev builds of P/9, Q/10, or R/11 before attempting to install this release (however this can be flashed over an existing official Q/10 install)
* Ensure SD is formatted as FAT32 or exFAT. It will be formatted automatically as FAT32 during partitioning as it is necessary to boot Android (or L4T Linux)
* `hekate` >= 6.0.3 is REQUIRED, so please update BEFORE attempting an upgrade
  * `hekate` >= 6.0.5v2 is needed for some >= Beta 2 features

### Steps

1. Download the required files and put hekate on the SD card\
   a. Download the latest version of the `hekate` bootloader from [HERE](https://github.com/CTCaer/hekate/releases) (`hekate_ctcaer_[version]_Nyx_[version].zip`)\
   b. Unzip the `hekate` archive to the root of the SD card\
   c. Download the latest zip [HERE](https://download.switchroot.org/android-11/) of either `nx-tab` (standard tablet Android) or `nx-atv` (Android TV)

Your SD card should now look like this, assuming it is shared with HOS (Horizon OS/Switch OS):

```
root
|- bootloader
|  |- ini
|  |  |- ...
|  |- payloads
|  |  |- ...
|  |- res
|  |  |- ...
|  |- sys
|  |  |- ...
```

2. Prepare Switch, boot into hekate, and dump JoyCon pairing info\
   a. Boot into HOS and ensure Joy-Cons are paired\
   b. Insert your SD card into your Switch\
   c. (Unpatched v1 only) Slide the RCM jig into the right rail, and hook your Switch up to your PC (or other payload injector) via USB-C and power off the console\
   d. (Unpatched v1 only) Press Power and VOL + simultaneously to enter RCM mode and inject the hekate payload (`hekate_ctcaer_[version].bin`) that came with your downloaded hekate zip\
   NOTE: To keep it simple you can use [this](https://switch.exploit.fortheusers.org/) in Google Chrome (not compatible with other browsers), check "Upload .bin payload" and select the payload mentioned above, then click "Do the thing!" and select "APX" from the pop-up menu.\
   e. Once booted into `hekate`, you can remove the RCM jig, and launch Nyx Settings

<figure><img src="../../.gitbook/assets/a11-1.jpg" alt=""><figcaption></figcaption></figure>

f. Dump your Joy-Con BT pairing information (with your Joy-Cons railed)

<figure><img src="../../.gitbook/assets/a11-2.jpg" alt=""><figcaption></figcaption></figure>

g. Ensure the message is correct, otherwise physically re-connect and re-pair the JoyCons in HOS and try again

<figure><img src="../../.gitbook/assets/a11-3.jpg" alt=""><figcaption></figcaption></figure>

3. Partition SD card in hekate\
   a. Navigate to `hekate`'s partition manager (on the Tools tab)

<figure><img src="../../.gitbook/assets/a11-4.jpg" alt=""><figcaption></figcaption></figure>

b. Use sliders to select appropriate space for each OS (we recommend at least 16 GB for Android, although it should be usable with \~8GB), then press Next Step\
NOTE: this step creates multiple partitions for a somewhat standard AOSP layout -- look [HERE](../../documentation/partition-docs.md) for more information if interested\
c. You may want backup Nintendo folder or any CFW files if necessary via `hekate`'s SD UMS tool over USB-C or by removing the SD card and reading directly on a PC (`hekate`/Nyx can pause and reload when SD is removed), but `hekate` will offer to backup and restore up to 1 GB of data from your existing file-system.

4. Add ROM zips to SD\
   a. Plug the Switch into your PC and select "SD UMS"\
   b. Extract the `nx-tab` or `nx-atv` `.7z` to the root of the SD card

Your SD card should now look like this:

```
root
|- bootloader
|  |- ini
|  |  |- ...
|  |- payloads
|  |  |- ...
|  |- res
|  |  |- ...
|  |- sys
|  |  |- ...
|- Nintendo (if you use stock)
|  |- ...
|- switchroot
|  |- android
|  |  |- ...
|  |- install
|  |  |- ...
|- lineage-18.1-[date]-UNOFFICIAL-[device].zip
```

5. Add other zips and enter recovery\
   a. Google Play and GMS minimal are now included--do NOT flash other Gapps\
   b. If you have any other flashable zips, place them on the root of the SD card\
   c. Safely remove/eject the SD UMS device from your computer\
   d. Back on the Switch, select "Flash Android" and accept the prompt to reboot to recovery\
   e. Select Factory Reset, and format data and system (NOTE: an "error" will print about the cache partition (fs type unsupported)--this is a soft error and should be ignored)\
   f. Go back, select "Apply Update", then "Choose from SWITCH SD"\
   g. Install the Lineage zip and any other zips you may have to install, and select Reboot -> System (NOTE: signature verification errors may occur and can be ignored)
6. First boot\
   a. Your JoyCons may not auto-pair on first boot--reboot to auto-pair your dumped JoyCons in Android
7. Subsequent boots\
   a. Boot Lineage 18.1 from More Configs --> Switchroot Android 11 in `hekate`\
   b. Hold VOL + on choosing this option to get back to recovery to flash zips\
   c. Hold VOL - on choosing this option to get back to `hekate` without having to reinject a payload\
   d. Rebooting the Switch will take you back to stock firmware
8. Asking for support/If you get stuck\
   a. First, get a screenshot of SD Info in `hekate`--this will allow us to rule out the most common issue first\
   b. If you are experiencing seemingly unintended behavior while already booted into Android, try and get us a log--find a guide on how to install `adb`, enable USB debugging in developer options on the Switch, plug into the computer with adb installed, and run `adb logcat > switchroot.log`. If this seems like too much for you, just get a bug report from developer settings.\
   c. Submit the screenshot and logs if needed to in our Discord server--the invite link can be found [HERE](https://discord.gg/N9PPYXjWMY).

### Switch Configuration App

The old Device and Performance Configuration menu has been rebranded into the Switch Configuration app, which brings with it many improvements for the new release.

#### Display Settings

As in previous releases, we have a resolution selecter, but now preferences are properly adhered to across docking and undocking loops. Choose a profile with a resolution, refresh rate, and colorimetry setting that works for you. A setting has been added to choose whether the internal panel should turn off when docked. Additionally, OLED units can select hardware panel color modes, including a Vivid preset and various Night modes.

#### Nintendo Controller Presets <a href="#nintendo-controller-presets" id="nintendo-controller-presets"></a>

In the Switch Configuration app/Settings menu, you can now enable some JoyCon/ProCon/etc. modifications without a flashable zip

* The Analog Trigger Emulation toggle enables or disables `LTRIGGER` and `RTRIGGER` axis emulation--when disabled, the `BUTTON_L2` and `BUTTON_R1` keys register instead--this can help troubleshoot/solve weird trigger behavior in different apps
* The Xbox Layout toggle swaps between the stock Nintendo ABXY button layout and the Xbox-style layout
* More to come!

#### Performance Mode and Overclocking

We use NVIDIA's `nvcpl` userspace service in conjunction with U-Boot scripted modification of the device tree to set max clocks on the CPU and GPU, with higher limits than Android Q had. To improve this along with the overall experience on Mariko consoles, we have introduced overclocking profiles. Each profile has **Perf Mode Docked** (A), **Perf Mode Handheld/Stock Docked** (B), and **Stock Handheld** (C) clocks. Perf mode is enabled via the toggle in Switch Configuration. Different profiles are enabled using the new `oc` ini option, but they sometimes require (or are different with) the `dvfsb`, `gpu_dvfsc`, and `limit_gpu_clk` ini options. RAM OC is handled in bootloader and information on this can be found on the `hekate` GitHub page, linked above.

WARNING: These profiles are not all tested, and there may be issues with some of them. Plus, extreme overclocking can damage your device, so be cautious. Overclocks >1 are considered unstable on increasingly higher-binned chips (better binnings will work on higher OC levels without frequency throttling or becoming unstable). Overclocking is not recommended, but is there for those who want to experiment.

How to read: columns are entries in `bootloader/ini/android.ini`, with the values indicated in each row. "N/A" indicates that the key has no effect, while "any" indicates that the key of that column is irrelevant for that clock profile with the other values. Entries of the form ">$number" indicate any value greater than $number, for example ">0". `dvfsb` and `gpu_dvfsc` are voltage/frequency scaling curve modifiers for better-binned hardware. Some profiles require this. Read the [INI Guide](11-r-ini-guide.md) for details.

NOTE: these are _userspace limiters_ for clocks, not guarantees or manual freq targets. Kernel etc. will limit more if your binning does not support the given clocks.

#### CPU Frequency Matrix (MHz)

| sku     | oc | dvfsb | A    | B    | C    |
| ------- | -- | ----- | ---- | ---- | ---- |
| any     | 0  | N/A   | 1785 | 1785 | 1428 |
| v1      | 1  | N/A   | 1887 | 1887 | 1785 |
| v1      | 2  | N/A   | 1989 | 1989 | 1785 |
| v1      | 3  | N/A   | 2091 | 2091 | 1785 |
| Lite    | 1  | any   | 1963 | 1963 | 1785 |
| Lite    | 2  | 1     | 2091 | 2091 | 1785 |
| v2/OLED | 1  | any   | 1963 | 1963 | 1785 |
| v2/OLED | 2  | any   | 2091 | 2091 | 1785 |
| v2/OLED | 3  | any   | 2295 | 2295 | 2091 |
| v2/OLED | 4  | 1     | 2397 | 2397 | 2091 |

#### GPU Frequency Matrix (MHz)

<table><thead><tr><th width="115">sku</th><th width="70">oc</th><th width="87">dvfsb</th><th width="114">gpu_dvfsc</th><th width="136">limit_gpu_clk</th><th>A</th><th>B</th><th>C</th></tr></thead><tbody><tr><td>v1</td><td>any</td><td>N/A</td><td>N/A</td><td>N/A</td><td>921</td><td>768</td><td>461</td></tr><tr><td>Lite</td><td>any</td><td>0</td><td>N/A</td><td>N/A</td><td>768</td><td>768</td><td>461</td></tr><tr><td>Lite</td><td>>0</td><td>1</td><td>N/A</td><td>N/A</td><td>844</td><td>844</td><td>768</td></tr><tr><td>v2/OLED</td><td>0</td><td>any</td><td>any</td><td>any</td><td>921</td><td>768</td><td>461</td></tr><tr><td>v2/OLED</td><td>1</td><td>0</td><td>0</td><td>any</td><td>999</td><td>999</td><td>922</td></tr><tr><td>v2/OLED</td><td>>1</td><td>1</td><td>any</td><td>any</td><td>1075</td><td>1075</td><td>999</td></tr><tr><td>v2/OLED</td><td>>1</td><td>any</td><td>any</td><td>1</td><td>1075</td><td>1075</td><td>999</td></tr><tr><td>v2/OLED</td><td>>1</td><td>0</td><td>1</td><td>0</td><td>1229</td><td>1229</td><td>999</td></tr><tr><td>v2/OLED</td><td>>1</td><td>0</td><td>0</td><td>0</td><td>1267</td><td>1267</td><td>999</td></tr></tbody></table>

### Updating

Updates are pushed through OTA (over-the-air) updates, just like any standard Android OEM. Use the Updater in Settings to get the latest updates without losing data.

### Tips and Tricks

* **TWRP** is available from the [Switchroot download site](https://download.switchroot.org/android-11/extras/)
  * You must manually uncheck the box for upgrading Lineage Recovery alongside the OS, either during setup or in the triple-dot menu on the Updater page in Settings
  * To flash the `twrp.img`, put it in `switchroot/install` and hit "Flash Android" in `hekate` partitioner again
  * This will not overwrite your install, just your recovery
* The [Nvidia Shield TV app](https://play.google.com/store/apps/details?id=com.nvidia.shield.remote) is a **must have for docked usage**, allowing keyboard and ~~mouse~~ (touchpad is currently broken) control from a phone.
* If you don't set a password the switch will auto-unlock after being docked.
* The capture button on the Joy-Cons is mapped to Play/Pause media.
* You can enable **desktop mode** to have a slightly better docked experience by going to Developer Options and scrolling down to 'Force Desktop Mode' enable that and reboot.
* To see **battery percentage on ATV** follow [this guide.](https://gist.github.com/bylaws/6db669bb048c1207c3309235ce42c92c)
* For **root** the [latest magisk](https://github.com/topjohnwu/Magisk/releases) can be flashed in recovery
  * This currently breaks docked audio--this is a Magisk issue and not ours
* To transfer files to Android you can use MTP over USB (currently requires USB3 to be enabled).

### Changelog

#### Beta 2.75

* Fixed CEC double input
* Fixed Lite calibration
* EMC table fixes from L4T
* Better MAC address parsing (and fixed Wi-Fi MAC--now BT MAC with FF as last byte is used, and only the joycon\_mac.ini is parsed)
* Console Launcher version has been bumped to 107

#### Beta 2.5

* Fixed Erista fans

#### Beta 2

* Added support for new L4T features like parsing dumped Lite calibration data
* Actually added ddr200 for microSD cards (disabled by default unlike on L4T due to possible incompatibility with some cards) and a few other new ini options
* Erista fans now actually fire up and should use proper profiles
* U-Boot vidconsole should work now (customized config options and errors during this stage are printed to console as they are parsed)
* ATV audio should be fixed (duplicated tab audio policy instead of using NVIDIA default ATV policy)
* Wireguard is actually built in
* Assorted bugfixes

#### Beta 1

* Updated to Android 11/LineageOS 18.1
* Switch to Lineage Recovery as default recovery (TWRP available as standalone flashable image on download page)
* Support for all Mariko models (v2, Lite, OLED)
* Migrated bootloader to L4T-Loader
* Built-in GMS
* Support for desktop GL
* Full hardware composition/overlays (including when docking)
* Overclocking profiles added via hekate ini entries (see wiki for details)
* JoyCons work in more apps now (including GeForce Now)
* Updated Shield stock content, firmware, etc. to Shield Experience 9.1.1
* Massively improved Wi-Fi support and consistency (no more channel workaround)
* Improved DeviceSettings (now Switch Configuration) with JoyCon config options to replace zips like joycon-xbox and new panel options
* Docking supported with or without internal panel mirroring
* [Console Launcher](https://www.consolelauncher.app/) built in, with Pro upgrade included for free for all Switchroot users (Thanks @TheKyle#8441!)
* Touch and controller keyboards built in on both ATV and tablet distributions
* Switchroot themeing, including background and some color scheme elements
* Fixes for some button registration on Nintendo controllers in different apps
* Most of the kernel-side improvements in L4T also apply here
* Many more QoL changes and fixes--read the commit logs for more info

### Known Bugs

* Docking sometimes requires undocking and redocking and some weird displays sometimes don't work--this will improve with updates
* Docking sometimes defaults to a stupid resolution--just adjust in the Switch Configuration app
* Sensor HAL (gyro, accel, etc.) is broken on all devices--no support is planned for the Invensense IMU found in the OLED, but the STM ones should work--not sure why they don't
* Volume UI does not work due to platform bug
* RSMouse is not included
* Shield TV Remote trackpad does not work
* In-place upgrades from Q are supported but can be unstable--we recommend clean installs
* Joycons do not turn off in sleep automatically, make sure to turn them off by tapping the 'sync' button on side.

### Maintainer

makinbacon - [https://paypal.me/makinbacon21](https://paypal.me/makinbacon21)
