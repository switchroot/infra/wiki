# 11/R Build Guide

This guide covers building of Switchroot Android 11/R, based on LineageOS 18.1, with Switchroot + Lineage's `nx` device repo and LineageOS mainline trees.

**This is a GUIDE, not a SCRIPT--read and understand, don't just copy-paste**

We recommend inexperienced users do not build this. Android 11 is not yet ready for release or public consumption. The source trees may have missing bits or be incomplete, issues will be present until (and through) the final release, and we do not provide any support for self-compiled installs (and they will not receive OTA updates).

Note that while this isn't ready for use at the moment, it does support all Switch models.

### PSA and Licensing <a href="#psa-and-licensing" id="psa-and-licensing"></a>

These repos are licenses under the [Apache License, version 2.0](https://www.apache.org/licenses/LICENSE-2.0), for all AOSP, LineageOS Project, and Switchroot (via personal copyright) copyright-labelled files, and the Linux kernel (along with some drivers) is licensed under the [GNU General Public License, version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html). Attribution and/or patches are required to be published as per each license.

### Requirements <a href="#requirements" id="requirements"></a>

* \~350 GB free storage
* \~16GB RAM
* Ubuntu Linux installation (WSL2 works as well, but the i/o speeds are generally extremely poor)

### Environment Setup <a href="#environment-setup" id="environment-setup"></a>

```bash
# Grab platform tools & add to path
export BUILDBASE=[path-to-build-root]
wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip platform-tools-latest-linux.zip -d ~
cat >> ~/.profile<< EOF
# add Android SDK platform tools to path
if [ -d "\$HOME/platform-tools" ] ; then
    PATH="\$HOME/platform-tools:\$PATH"
fi
EOF

# Update and grab new packages
sudo apt update
sudo apt upgrade
sudo apt install bc bison build-essential ccache curl flex g++-multilib gcc-multilib git git-lfs gnupg gperf imagemagick lib32ncurses5-dev lib32readline-dev \
lib32z1-dev liblz4-tool libncurses5 libncurses5-dev libsdl1.2-dev libssl-dev libxml2 libxml2-utils lzop pngcrush python3 rsync schedtool squashfs-tools \
xsltproc zip zlib1g-dev

# Needed on some versions of Ubuntu
sudo apt install python-is-python3

# Only needed for Ubuntu < 20.04:
sudo apt install libwxgtk3.0-dev python-is-python3

# Only needed for Ubuntu < 16.04:
sudo apt install libwxgtk2.8-dev

# Create folders
mkdir -p ~/bin
mkdir -p $BUILDBASE/android/lineage

# Grab repo command
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo

# Only needed for older versions of Ubuntu:
cat >> ~/.profile<< EOF
# set PATH so it includes user's private bin if it exists
if [ -d "\$HOME/bin" ] ; then
    PATH="\$HOME/bin:\$PATH"
fi
EOF

# The below must be run on all version of Ubuntu, regardless of whether bin is added to path.
source ~/.profile

# Configure Git
git config --global user.email "[email protected]"
git config --global user.name "Your Name"
```

Copy

### Clean Build <a href="#clean-build" id="clean-build"></a>

```bash
# Init repo tree and grab Switchroot 18.1 manifest
cd $BUILDBASE/android/lineage
repo init -u https://github.com/LineageOS/android.git -b lineage-18.1 --git-lfs
git clone https://gitlab.com/switchroot/android/manifest.git --recursive -b lineage-18.1 .repo/local_manifests
.repo/local_manifests/snack/snack.sh -y
export USE_CCACHE=1
export CCACHE_EXEC=$(which ccache)
export WITHOUT_CHECK_API=true
ccache -M 50G

source build/envsetup.sh

# For standard tablet Android
lunch lineage_nx_tab-userdebug

# For Android TV
lunch lineage_nx-userdebug

# Build
mka bacon
```

Copy

#### First Install <a href="#first-install" id="first-install"></a>

PLEASE WIPE ANY 17.1 OR OLD 18.1 INSTALLS BEFORE TRYING TO INSTALL

1. Grab the latest version of the `hekate` bootloader from [HERE](https://github.com/CTCaer/hekate/releases) and unzip it to the root of the SD card-you NEED 6.0.3 or later.
2. From `out/target/product/[device name]`, copy `00-android.ini` to `bootloader/ini/`, `bl31.bin` and `bl33.bin` to `switchroot/android`, `boot.img`, `recovery.img`, and `install/nx-plat.dtbimg` to `switchroot/install`, and `lineage-18.1-[date]-UNOFFICIAL-[device name].zip` to the root of the SD card.
3. Flash Android in `hekate`'s partition manager
4. Continue to Lineage Recovery and install the `lineage...` zip

### Updating Tree <a href="#updating-tree" id="updating-tree"></a>

```bash
cd $BUILDBASE/android/lineage

# ONLY IF YOU HAVEN'T BUILT FOR A WHILE
repo init --git-lfs

.repo/local_manifests/snack/snack.sh -y # Use -n instead to skip syncing and
                                                                                # -w to not overwrite local changes
export USE_CCACHE=1
export CCACHE_EXEC=$(which ccache)
export WITHOUT_CHECK_API=true
ccache -M 50G

source build/envsetup.sh

# For standard tablet Android
lunch lineage_nx_tab-userdebug

# For Android TV
lunch lineage_nx-userdebug

# Build
mka bacon
```

Copy

#### Updating Install <a href="#updating-install" id="updating-install"></a>

PLEASE WIPE ANY 17.1 OR OLD 18.1 INSTALLS BEFORE TRYING TO INSTALL

1. From `out/target/product/[device name]`, copy `lineage-18.1-[date]-UNOFFICIAL-[device name].zip` to the root of the SD card
2. Boot to Lineage Recovery by holding VOL+ on boot and install the new `lineage...` zip
