# 14, 15 Partition Docs

Android 14 and 15 utilize a [dynamic partition](https://source.android.com/docs/core/ota/dynamic_partitions) scheme enabling resizing of system partitions via updates, newer updater support, and more. This has allowed us to push the Switch to further Android releases since the removal of System-as-Root in Android 14.

Required partitions and sizes (order should not matter but it is best to keep in order for semantics, and the GPT table must come after the MBR table):

<table><thead><tr><th width="171">Partition</th><th width="547.3333333333333">Name</th><th>Size</th></tr></thead><tbody><tr><td>Boot (Linux Kernel)</td><td>boot</td><td>64MiB</td></tr><tr><td>Recovery</td><td>recovery</td><td>64MiB</td></tr><tr><td>Device Tree Reference</td><td>dtb</td><td>1MiB</td></tr><tr><td>Miscellaneous</td><td>misc</td><td>3MiB</td></tr><tr><td>Cache</td><td>cache</td><td>60 MiB</td></tr><tr><td>Super</td><td>super</td><td>5922 MiB</td></tr><tr><td>Userdata</td><td>userdata</td><td>Custom*</td></tr></tbody></table>

( 1 GiB = 1024 MiB )

\* If making Android partitions by hand, take into account that if you have no emuMMC partition (which would go after the Android userdata partition) you should reserve 1MB of unallocated space at the end of the partition table.

Data is based off [`hekate`'s partitioner source code](https://github.com/CTCaer/hekate/blob/master/nyx/nyx_gui/frontend/gui_tools_partition_manager.c).
