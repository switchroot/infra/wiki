---
description: Helpful links and descriptions for Android 14 and 15 for Switch
---

# Android 14, 15

Switchroot Android 14 (Q) and 15 (V) are fully upstream LineageOS 21 and 22.1 releases supporting all SKUs (`odin`/v1, `modin`/v2, `vali`/Lite, `fric`/OLED)

Kernel: Downstream Switchroot L4T kernel, rebased on rel-shield-r kernel-4.9 and r35 kernel-nvidia, kernel-nvgpu

Blobs: Shield Experience 9.1.1



## Installing

To install official release or install, please follow the official LineageOS guide for your device:

Tablet: [https://wiki.lineageos.org/devices/nx\_tab/](https://wiki.lineageos.org/devices/nx_tab/)

Android TV: [https://wiki.lineageos.org/devices/nx/](https://wiki.lineageos.org/devices/nx/)

For an eMMC install (supported but not official), follow our [14-15-emmc-boot-guide.md](14-15-emmc-boot-guide.md "mention")

## Customization

To customize, see the [11-r-ini-guide.md](11-r-ini-guide.md "mention")--note that enabling DDR200 can cause issues for unsupported cards, but is enabled by default on all Samsung.

More details coming soon!

## Overclocking

As of now, only RAM overclocking is supported. Future add-ons will deliver full overclocking.

### Maintainers

makinbacon - [https://paypal.me/makinbacon21](https://paypal.me/makinbacon21)

npjohnson - [https://paypal.me/nolenjohnson](https://paypal.me/nolenjohnson)
