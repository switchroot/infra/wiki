# 14/U eMMC Boot Guide

### Prerequisite <a href="#pre-requisite" id="pre-requisite"></a>

* Full eMMC Backup (boot0/1 and rawnand.bin)
* PC running linux

<mark style="color:red;">**WARNING: Flashing android to your eMMC can destroy your SwitchOS if you dont make a proper backup of your eMMC beforehand like I told in the requirements. You can backup your eMMC in Hekate.**</mark>

### Installation <a href="#installation" id="installation"></a>

#### eMMC preparation <a href="#emmc-preparation-for-android-and-linux-alongside-hos-on-a-larger-emmc" id="emmc-preparation-for-android-and-linux-alongside-hos-on-a-larger-emmc"></a>

1. Erase your eMMC or shrink the USER partition
2. In hekate go to USB Tools -> Set Read-Only to OFF -> Mount eMMC RAW GPP
3. In a new Terminal run `sudo gdisk /dev/<disk>` (where \`\<disk>\` is your the mounted eMMC)
4. x (enter expert mode) -> s (resize partition table) -> 128 (set max entries to 128) -> w (write changes to disk) -> y (confirm write)
5. In Gparted open your disk and create the following partition in the same order



| Partition Name | Size    | File system |
| -------------- | ------- | ----------- |
| boot           | 64MiB   | unformatted |
| recovery       | 64MiB   | unformatted |
| dtb            | 1MiB    | unformatted |
| misc           | 3MiB    | unformatted |
| cache          | 60MiB   | unformatted |
| super          | 5922MiB | unformatted |
| userdata       | Custom  | ext4        |

#### Flashing the eMMC <a href="#flashing-the-emmc" id="flashing-the-emmc"></a>

1. In a terminal use dd to flash the following partition accordingly e.g. `sudo dd if=boot.img of=/dev/<disk><boot_partition_number> bs=32M`

| File            | GPT Name |
| --------------- | -------- |
| `boot.img`      | boot     |
| `recovery.img`  | recovery |
| `nx-plat.dtimg` | dtb      |

2. Create the following file on your SD Card in `bootloader/ini/android.ini`

```
[LineageOS]
l4t=1
boot_prefixes=switchroot/android/
id=SWANDR
icon=switchroot/android/icon_android_hue.bmp
logopath=switchroot/android/bootlogo_android.bmp
r2p_action=self
usb3_enable=1
emmc=1
```

3. The rest of the guide can be followed as-is
