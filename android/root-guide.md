# Root Guide

**NOTE**: Rooting, while allowed, means you will receive 0 support. This process modifies your boot image (kernel) and other pieces of Android. Do not ask for help with a bug, crash, etc. if you have Magisk or any other root solution installed. The LineageOS Project follows the same guideline.&#x20;

## What is Magisk

Magisk, or "The Magic Mask" gives full root access to users of your device and any applications you have installed. There are other root solutions, but they generally require more extensive patching and newer kernel versions, plus are less broadly supported in general.

## Tutorial

Rooting your Switch is much like rooting other Android devices, although starting the process by sideloading the app is unsupported. To install, download the latest Magisk release from GitHub ([https://github.com/topjohnwu/Magisk/releases](https://github.com/topjohnwu/Magisk/releases)) and rename the `.apk` to `.zip`. Now, you can place the file on the root of the SD card FAT32 partition and flash the zip in recovery.

If you have TWRP installed (supported for Android 8.1 through 11), select "Install" and choose the zip.

If you have Android Recovery (default on Android 11+), select "Apply Update" and choose the zip. You may be prompted about signature check failures; just continue anyway (this is to point out this is not an official Lineage zip, as with GApps etc.).

You can also use ADB Sideload for either, similar to the default install process for 14/15.

