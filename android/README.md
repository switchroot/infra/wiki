---
description: LineageOS-based Android distribution for the Nintendo Switch
layout:
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: false
  pagination:
    visible: false
---

# Android

## Distributions

[android-14-15](android-14-15/ "mention")

[android-11](android-11/ "mention")

[android-10](android-10/ "mention")

[android-8.1.md](android-8.1.md "mention")

## Extras

[root-guide.md](root-guide.md "mention")
