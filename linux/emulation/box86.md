---
description: Guide to install and setup Box86 on Fedora L4T
---

# Box86

**Introduction**

Running Linux version of Steam using box86 using chroot and install Wine.\
Note: Box86 uses sofware multithreaded-cpu based GPU emulation.

Parts of this guide are based on this article by rna from armbian forum [https://forum.armbian.com/topic/16584-install-box86-on-arm64/](https://forum.armbian.com/topic/16584-install-box86-on-arm64/)

<mark style="color:red;">**DISCLAIMER: This guide is only intended for L4T Fedora users. Ubuntu users should use the L4T Megascript to install box64 and box86.**</mark>

### Setting up

#### 1. Host side

We need `debootstrap`to create the chroot and schroot to enter the chroot. We will also need to install `virgl-test-server` to have GPU acceleration.

1. Install build dependencies

```
sudo dnf install --refresh -y debootstrap arch-install-scripts virglrenderer-test-server xorg-x11-server-Xephyr
```

2. Create chroot directory

```
sudo mkdir -p /srv/chroot/debian-armhf
```

3. Create the chroot using debootstrap

```
sudo debootstrap --include=nano,sudo,git,wget,mesa-vulkan-drivers,libglx-mesa0,libegl-mesa0,libglapi-mesa,libgl1,zenity,nginx,xterm,ca-certificates,gnupg,libnm0 --arch armhf --foreign testing /srv/chroot/debian-armhf http://deb.debian.org/debian
```

4. Run debootstrap second stage

```
sudo chroot /srv/chroot/debian-armhf /debootstrap/debootstrap --second-stage
```

**2. Chroot side**

Now you should be able to schroot by typing

```
sudo arch-chroot /srv/chroot/debian-armhf
```

1. Edit the chroot's default environment. You are free to set another display number (`DISPLAY=`) for Xephyr (starting from :2 when using Xephyr).

```
nano /etc/profile.d/steam.sh
```

Append the following variables at the end of the file then save:

```
export LANGUAGE="C"
export LC_ALL="C"
export DISPLAY=:0
export STEAMOS=1
export STEAM_RUNTIME=1
```

2. Fix apt and install box86

```
apt update -y && apt -f -y install && apt upgrade -y
echo 'deb [signed-by=/usr/share/keyrings/box86-debs-archive-keyring.gpg] https://itai-nelken.github.io/weekly-box86-debs/debian/ /' > /etc/apt/sources.list.d/box86.list
wget -qO- https://itai-nelken.github.io/weekly-box86-debs/debian/KEY.gpg | gpg --dearmor -o /usr/share/keyrings/box86-debs-archive-keyring.gpg
apt update && apt install -y box86 libxss1 libpulse-dev libsndfile1-dev libsdl2-image-2.0-0 libsdl2-net-2.0-0 libsdl2-2.0-0 libc6 libx11-6 libstdc++6 libopenal1 osspd libjpeg62 libudev1
```

3. Let's exit the chroot first, the next commands need to be run on host

```
exit
```

4. Before we proceed further in chroot, we need to copy binfmt.d config from chroot to host and restart systemd-binfmt to make box86 works automaticly with bash scripts that includes i386 files.

```
sudo cp /srv/chroot/debian-armhf/etc/binfmt.d/box86.conf /etc/binfmt.d/box86.conf
sudo systemctl restart systemd-binfmt
```

5. Create your `<username>` and grant it sudo privileges

```
sudo arch-chroot /srv/chroot/debian-armhf
echo "<username> ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers
useradd -u 1000 --shell /bin/bash -rmUG sudo,video,audio,render <username>
exit
```

#### Steam

Be sure that virgl-server is still running, then run :

```
sudo arch-chroot /srv/chroot/debian-armhf /bin/su - <username>
wget https://steamcdn-a.akamaihd.net/client/installer/steam.deb
sudo apt install -y ./steam.deb
setarch -L linux32 steam -no-browser -noreactlogin -no-cef-sandbox steam://open/minigameslist
```

You should be able to run steam in the chroot using the following command :

```
setarch -L linux32 steam -no-browser -noreactlogin -no-cef-sandbox steam://open/minigameslist
```
