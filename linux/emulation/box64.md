---
description: x64 emulator for arm64
---

# Box64

## Box64 <a href="#box64" id="box64"></a>

### Introduction <a href="#introduction" id="introduction"></a>

Box64 is a x64 ISA compatibility layer for 64bit platforms.\
Box64 has GPU accelerated OpenGL, OpenGLES, EGL, and Vulkan

Source code is available [here](https://github.com/ptitSeb/box64/)

### Prebuilt APT repo <a href="#prebuilt-apt-repo" id="prebuilt-apt-repo"></a>

Installing `Box64` via the [L4T-Megascript](https://github.com/cobalt2727/L4T-Megascript/wiki) or [Pi-Apps](https://github.com/Botspot/pi-apps) will add an apt repository with optimized Box64 built daily.

### Compiling from Source (for Box64 development) <a href="#compiling-from-source-for-box64-development" id="compiling-from-source-for-box64-development"></a>

Full compiling instructions are available [here](https://github.com/ptitSeb/box64/blob/main/COMPILE.md).

```
git clone https://github.com/ptitSeb/box64
cd box64
mkdir build; cd build; cmake .. -DTEGRAX1=1 -DCMAKE_BUILD_TYPE=RelWithDebInfo
make -j4
sudo make install
```

If it's the first install, you also need:

```
sudo systemctl restart systemd-binfmt
```
