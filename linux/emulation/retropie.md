---
description: >-
  Retropie allows you to turn your Nintendo Switch (and other supported devices)
  into a retro-gaming machine.
---

# RetroPie

## RetroPie <a href="#retropie" id="retropie"></a>

[Retropie](https://github.com/RetroPie/RetroPie-Setup) allows you to turn your Nintendo Switch (and other supported devices) into a retro-gaming machine. It builds upon L4T Ubuntu Bionic, EmulationStation, RetroArch and many other projects to enable you to play your favorite Arcade, home-console, and classic PC games with the minimum set-up. For power users it also provides a large variety of configuration tools to customize the system as you want.\
\
Original web page: [https://retropie.org.uk/](https://retropie.org.uk/)\
\
[List of tested cores/emulators](https://github.com/cobalt2727/L4T-Megascript/wiki/Emulator-compatibility-list-\(standalone-&-libretro\))\
\
[Binaries Included in the Megascript Repo](https://github.com/theofficialgman/RetroPie-Binaries/tree/master/Binaries/tegra-x1/libretrocores)

Installation is simple with the [L4T-Megascript](https://github.com/cobalt2727/L4T-Megascript/wiki/Initial-Setup) and can be quite fast, usually less than an hour, thanks to the new Megascript supplied Core binaries for most of the cores.

RetroPie provides their own documentation for setting up controllers, found [here](https://retropie.org.uk/docs/Controller-Configuration/)

The Megascript implements an arbitrary game detection script for RetroPie. Any games you install on your system that show in the standard applications list under the games category will be detected and added to the ports menu in RetroPie. (This is accomplished by custom python and bash scripts which run on every shutdown/restart of emulationstation)

Power users may have the desire to install more cores than installed by the Megascript by default. Its recommended that you use the Megascript install due to custom features and fixes that it includes. After it has installed you can navigate the RetroPie setup script as normal to install more cores as seen here:

```
cd ~/RetroPie-Setup
sudo ./retropie_setup.sh
```
