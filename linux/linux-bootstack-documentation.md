# Linux Bootstack Documentation

## Early Bootstack <a href="#early-bootstack" id="early-bootstack"></a>

This section and subsections describe how the switch bootstack works.

### hekate <a href="#hekate" id="hekate"></a>

It is the first stage bootloader used to configure, flash and is responsible for booting L4T on Switch.\
By not going into details, hekate itself falls in the "payload" category of the switch hacking scene and is a requirement to boot L4T. Below are the L4T relevant features.

### L4T-Loader <a href="#l4t-loader" id="l4t-loader"></a>

L4T-Loader is the part of hekate that prepares the boot files and configuration and finally launches the next stage of Linux booting (ATF/UBOOT).\
Additionally it passes over all configuration keys of the selected .ini boot entry to said stage.\
`README_CONFIG.txt` explains these keys.

### SD Tools and Partition Manager <a href="#sd-tools-and-partition-manager" id="sd-tools-and-partition-manager"></a>

Used for backing and restoring files and partitions of the SD card.\
It also prepares the SD card with the relevant partitions that are gonna be used for the flashing process of any Switch Linux-based distro.

### JoyCon Bluetooth pairing information dump <a href="#joycon-bluetooth-pairing-information-dump" id="joycon-bluetooth-pairing-information-dump"></a>

hekate is able to dump the pairing information from the JoyCon, which are then used by Linux based systems to connect to the JoyCons without the need for pairing (thus losing the pairing done in Horizon).

## Late Bootstack <a href="#late-bootstack" id="late-bootstack"></a>

### ATF <a href="#atf" id="atf"></a>

It's the TrustZone (Security Monitor) firmware of any ARM based linux system.\
It also launches U-BOOT

### U-BOOT <a href="#u-boot" id="u-boot"></a>

It's the main Linux launcher firmware. It takes the configuration for hekate and loads Kernel, Initramfs and ARM Device Tree in order for Linux to boot.

### Kernel <a href="#kernel" id="kernel"></a>

Aka Linux. The core piece of anything that is called Linux or Linux-based.

### Initramfs <a href="#initramfs" id="initramfs"></a>

Normally used to do initial **userspace** configuration and select the correct drive to boot userspace.

### Userspace <a href="#userspace" id="userspace"></a>

Anything below Kernel. For example a Linux distribution or Android.

[Boostack Dev Documentation](https://gitlab.com/switchroot/bootstack/documentation)
