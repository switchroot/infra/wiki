---
description: USB or eMMC booting
---

# Linux USB or eMMC Boot

## Prerequisites <a href="#prerequisites" id="prerequisites"></a>

These guides are meant to augment the official installation guides.\
Follow the guide for your appropriate distro (Ubuntu Bionic/Ubuntu Jammy/Fedora) **EXCEPT** for repartitioning the SD card and clicking "Flash Linux" in Hekate.\
The following instructions are a replacement for the "Flash Linux" step in Hekate. (You can delete the files from sd:/switchroot/install).

## USB Boot <a href="#usb-boot" id="usb-boot"></a>

You will **need a mouse and keyboard to finish setup** if you are using a dock that covers the touchscreen.\
If your dock/adapter does not cover the screen and you don't have a mouse and keyboard, disconnect HDMI before booting for the first time, so you can finish the setup by using built-in screen and touch.\
You can also manage to pass the setup with a mouse only and using the on screen keyboard, but your mouse shouldn't be detected as keyboard also, otherwise the OSK will not show up.

1. Create a new partition on the usb drive - _the partition type doesn't matter_

**On Linux :**

2. Merge l4t split image into one: `cat l4t.0* > ubuntu.raw`
3. `sudo dd if=ubuntu.raw of=/dev/sdXY bs=2M conv=fsync status=progress` replacing X by the correct drive LETTER and Y by the partition NUMBER on this drive
4. Open `bootloader/ini/L4T_ubuntu.ini` located on your sd card fat32 partition\
   a. Add `rootlabel_retries=100`\
   b. **If there's another Ubuntu in SD** add `rootdev=sdaX`. Replace the X in sdaX with your partition number (1 if your USB drive only has the one partition) and change the partition label to avoid collision with the install on your SD, for example you can name the USB one to `SWR-USB` and make sure to also set `id=SWR-USB` in `L4T-Bionic.ini` or the relevant `.ini`
5. Finally add `usb3_enable=1` to the .ini file to enable max usb speeds. (This might decrease BT and WiFi 2.4G signal quality).

**On windows :**

2. Open command prompt, navigate to switchroot/install directory then merge l4t split image into one `copy /b l4t.00 + l4t.01 ubuntu.raw`
3. Download [dd for windows binary](http://www.chrysocome.net/dd) and extract dd-XXbetaX.zip.
4. Open a cmd/terminal in that folder and execute`dd --list` to list all your disks volumes. Cross reference windows "Disk Management" or the "DiskGenius" application to find your partition and copy its volume ID (looks something like `\\.\Volume{XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}`)

WARNING this next step will OVERWRITE the disk volume\
Be careful to select the right volume and not flash over your windows disk. Make sure to remove the drive letter of your desired partition before flashing otherwise you will get an error.

5. In your open cmd/terminal, to flash `ubuntu.raw` to a USB PARTITION, execute `dd if=C:\Users\myuser\pathtofile\ubuntu.raw of=\\.\Volume{XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX} bs=2M --progress`
6. Open `bootloader/ini/L4T_ubuntu.ini` located on your sd card fat32 partition\
   a. Add `rootlabel_retries=100`\
   b. **If there's another Ubuntu in SD** add `rootdev=sdaX`. Replace the X in sdaX with your partition number (1 if your USB drive only has the one partition) and change the partition label to avoid collision with the install on your SD, for example you can name the USB one to `SWR-USB` and make sure to also set `id=SWR-USB` in `L4T-Bionic.ini` or the relevant `.ini`
7. Finally add `usb3_enable=1` to the .ini file to enable max usb speeds. (This might decrease BT and WiFi 2.4G signal quality).

## eMMC Boot <a href="#emmc-boot" id="emmc-boot"></a>

**Note**: if you want to add a partition on the eMMC alongside HOS and/or Android [use this guide instead](../android/android-14-15/14-15-emmc-boot-guide.md)

1. Erase your eMMC or shrink the USER partition
2. In hekate go to USB Tools -> Set Read-Only to OFF -> Mount eMMC RAW GPP
3. In a new Terminal run `sudo gdisk /dev/<disk>` (where \`\<disk>\` is your the mounted eMMC)
4. x (enter expert mode) -> s (resize partition table) -> 128 (set max entries to 128) -> w (write changes to disk) -> y (confirm write)

**On Linux :**

5. Merge l4t split image into one: `cat l4t.0* > ubuntu.raw`
6. `sudo dd if=ubuntu.raw of=/dev/mmcblk0pX bs=2M conv=fsync status=progress` replacing X by the correct partition NUMBER on the eMMC
7. Open `bootloader/ini/L4T_ubuntu.ini` located on your sd card fat32 partition\
   a. Add `emmc=1` to enable eMMC driver\
   b. **If there's another Ubuntu in SD** add `rootdev=mmcblk0pX`. Replace the X in mmcblk0pX with your partition number (1 if your USB drive only has the one partition) and change the partition label to avoid collision with the install on your SD, for example you can name the USB one to `SWR-EMM` and make sure to also set `id=SWR-EMM` in `L4T-Bionic.ini` or the relevant `.ini`

**On windows :**

5. Open command prompt, navigate to switchroot/install directory then merge l4t split image into one `copy /b l4t.00 + l4t.01 ubuntu.raw`
6. Download [dd for windows binary](http://www.chrysocome.net/dd) and extract dd-XXbetaX.zip.
7. Open a cmd/terminal in that folder and execute`dd --list` to list all your disks volumes. Cross reference windows "Disk Management" or the "DiskGenius" application to find your partition and copy its volume ID (looks something like `\\.\Volume{XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}`)

WARNING this next step will OVERWRITE the disk volume\
Be careful to select the right volume and not flash over your windows disk. Make sure to remove the drive letter of your desired partition before flashing otherwise you will get an error.

8. In your open cmd/terminal, to flash `ubuntu.raw` to a USB PARTITION, execute `dd if=C:\Users\myuser\pathtofile\ubuntu.raw of=\\.\Volume{XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX} bs=2M --progress`
9. Open `bootloader/ini/L4T_ubuntu.ini` located on your sd card fat32 partition\
   a. Add `emmc=1` to enable eMMC driver\
   b. **If there's another Ubuntu in SD** add `rootdev=mmcblk0pX`. Replace the X in mmcblk0pX with your partition number (1 if your USB drive only has the one partition) and change the partition label to avoid collision with the install on your SD, for example you can name the USB one to `SWR-EMM` and make sure to also set `id=SWR-EMM` in `L4T-Bionic.ini` or the relevant `.ini`
