---
description: >-
  The Switch contains a unique combination of hardware allowing for a vast
  featureset not seen on many other devices. Commonly used programs/apps are
  listed and documented here for reference.
---

# Linux Features

## General Features Supported <a href="#general-features-supported" id="general-features-supported"></a>

* Switch 2017, Switch 2019, Switch Lite, Switch OLED support (Erista and Mariko)
* Full USB-C support\
  Power Delivery, On-The-Go, DisplayPort/HDMI on any adapters/dock
* Bluetooth and Wifi\
  BT controllers and audio SBC/AAC/AptX/Aptx HD support\
  WiFi with all channels except 2.4GHz 14 and 5GHz 144.
* Full Joy-Con Support (Excluding NFC, and IR camera)
* Nvidia Tegra GPU drivers - Vulkan, OpenGL, OpenGLES, EGL, CUDA
* Audio - Headphones, Speakers, HDMI (up to 5.1) when docked, and Microphone (version 3.4.2+)
* Touchscreen
* Handheld (720p @ 60) and Docked Display\
  Docked up to 4k30 or 1440p60 on Switch 2017/2019, and up to 4k60 on Switch OLED with OLED dock or 4k60 capable adapter
* CPU/GPU/RAM frequency scaling\
  Frequency depends on the [Nvidia Applet Power Mode, automatic Console and Handheld Profiles](linux-features.md#nvidia-power-profile-applet-nvpmodel)
* Automatic Overclocking through the Nvidia (Blue/Red Joy-Con) Indicator Applet (select an OC mode)
* Full eMMC/SD speed. For SD, proprietary UHS DDR200 is also supported.\
  DDR200 support makes Switch the first device in the world to even support it.
* Hardware Video acceleration\
  Support in system`ffmpeg` (including supported system applications like `MPV`, `SMPV`, and `VLC`), `firefox` ([from the firefox PPA on Ubuntu Jammy/Noble](https://launchpad.net/~mozillateam/+archive/ubuntu/ppa)), `ffmpeg-l4t` and `mpv-l4t` (Ubuntu Bionic), and Switchroot's`chromium-browser` (updates past 126 discontinued on all releases)\
  Watch videos without massive battery draining and convert videos really fast
* Deep Sleep
* IMU/Ambient Light sensor support
* R2P and R2C (Reboot to any hekate boot entry configuration)
* Support for Moonlight-QT (install with the L4T-Megascript)\
  Stream games/desktop from your NVIDIA/AMD/INTEL GPU (Sunshine Host or DEPRECATED NVIDIA Gamestream) to your Switch\
  HW video accelerated up to 4k
* [L4T-Megascript](https://github.com/cobalt2727/L4T-Megascript/wiki/Initial-Setup#installrun-the-megascript)
  * Use the [L4T-Megascript app](https://github.com/cobalt2727/L4T-Megascript/wiki), available in your apps list\
    You can get or build various essential stuff, apps or emulators with a single click. All configured and optimized for Switch.
* Widevine DRM support for Streaming platforms via `firefox` ([from the firefox PPA on Ubuntu Jammy/Noble](https://launchpad.net/~mozillateam/+archive/ubuntu/ppa)) and `chromium-browser`
* Updates distributed through the package manager (`apt` on Ubuntu and `dnf` on Fedora)
* Basically everything the hardware supports even if Switch OS doesn't support. Except NFC and IR.

### JoyCon and Switch Pro Controller native support <a href="#joycon-and-switch-pro-controller-native-support" id="joycon-and-switch-pro-controller-native-support"></a>

JoyCon may be paired through Linux blutooth settings or through hekate to allow for use in HOS, Linux, and Android without the need to repair.

JoyCon function via Bluetooth, railed on Switch, and connected via USB through the JoyCon charging grip. Multiple pairs can be connected and be registered by pressing L+R or ZL+ZR. Otherwise if SL+SR is used, it will pair them in sideways orientation.

Switch Pro Controller functions via Bluetooth and via USB. You may pair through the Linux/Android Bluetooth settings but the pairing will not match HOS.

To dump the pairing info for use on HOS and Linux without the need to re-pair follow these instructions:

> 1. Pair procon in HOS if you haven't already done that
> 2. Boot L4T on your Switch and connect procon via USB
> 3. Run `jc-dump-ltk` in the terminal to get the HOS LTK
> 4. Go to your sd card via the file manager at sd:/switchroot/ and edit or create `joycon_mac.ini`
> 5. If existing entries then add a `[joycon_02]` entry. Otherwise `[joycon_00]`.
> 6. It should look something like this:\
>    `[joycon_01]`\
>    `type=3`\
>    `mac=00:00:00:00:00:00`\
>    `host=00:00:00:00:00:00`\
>    `ltk=00000000000000000000000000000000`\
>    \
>    Description of the above keys:\
>    &#xNAN;**`type`** is **3** for Pro-Con\
>    &#xNAN;**`mac`** is the MAC from `jc-dump-ltk`\
>    &#xNAN;**`ltk`** is the LTK from `jc-dump-ltk`\
>    &#xNAN;**`host`** is your Switch Bluetooth MAC. Either copy it from existing entry or read it from `hekate` -> `Console Info` -> `Hw Info` -> `Cal0`\
>
> 7. Save and Reboot Linux. Now procon will be able to auto connect in both HOS and Linux.

Both JoyCons and Switch Pro Controllers have their IMU (accelerometer/gyro) exposed as udev inputs in Linux and can be used in games/programs with support (Dolphin-Emulator is a good example)

### Switch Docked Displayport/HDMI Output Support <a href="#switch-docked-displayporthdmi-output-support" id="switch-docked-displayporthdmi-output-support"></a>

Support for Official Nintendo Switch Dock as well as 3rd party Displayport/HDMI adapters/docks/hubs exists in Linux/Android. Official Dock requires power input from the official power adapter or better (15V 2.6A+ and 5V 1.5A+ PD profiles required from the charger). Many 3rd party adapters/docks/hubs can be used without supplemental power or with power from a USB-C PD Power adapter.

Official Original Nintendo Switch Dock is capable of resolutions up to 4k 30hz or 1440p 60hz. OLED dock supports up to 4k 60hz. Some 3rd party adapters can also achieve these resolutions. (note, any non-OLED Switch hardware is INCAPABLE of outputting 4k 60hz)

To specify a custom resolution for docked mode (useful for using 1920x1080 60hz on a 4K display): `echo "1920x1080" | sudo tee /etc/dock-hotplug.conf`

The syntax is basically `WIDTHxHEIGHT {optional xrandr settings}`. For example `1920x1080 --rate 120` will select 1080p at 120hz.

### Switch USB-C OTG Support <a href="#switch-usb-c-otg-support" id="switch-usb-c-otg-support"></a>

OTG via USB-C is enabled and devices can pull up to 1.3A when unpowered.\
Powered OTG adapters are not supported, it is advised to use a hub/dock (which usually contains HDMI/Displayport) if USB and powering is desired.

### Switch Accelerometer/Gyro and Rotation <a href="#switch-accelerometergyro-and-rotation-support" id="switch-accelerometergyro-and-rotation-support"></a>

Automatic rotation is supported in DE that have it enabled. The built in accelerometer/gyro is NOT exposed in userspace as a udev input.\
Instead, it is mapped as an iio device along with the ambient light sensor and fan tachometer.

```
/sys/bus/iio/devices/iio\:device0/ -> Accelerometer
/sys/bus/iio/devices/iio\:device1/ -> Rotation/IMU
```

***

## Common Features and Applications <a href="#common-features-and-applications" id="common-features-and-applications"></a>

### JoyCon Mouse/Keyboard mapping <a href="#joycon-mousekeyboard-mapping" id="joycon-mousekeyboard-mapping"></a>

Many users find it handy to map their JoyCons/Pro Controllers as a mouse/keyboard within Linux.\
There are profiles pre-installed for Official Joy-Cons, Switch Pro Controller, GameCube Controller (Wii-U/Switch Gamecube adapter with [ToadKing Driver](https://github.com/ToadKing/wii-u-gc-adapter)), Dualshock 4 Controllers (wired only, for now), X-Box One Controller (Wireless Only).

The current JoyCon/ProCon mapping is\\

<figure><img src="../.gitbook/assets/joycon_mapping.png" alt=""><figcaption></figcaption></figure>

If you are used to other mappings, give the above a try for a full week.\
It's based on the most common and intuitive gamepad mappings.\
If you still can't get used to it, view the `/usr/share/X11/xorg.conf.d/50-joystick.conf` file for instructions on editing/writing/customizing the configuration to your own needs.

### Reboot 2 Config <a href="#reboot-2-config" id="reboot-2-config"></a>

An app called `Reboot 2 Config` was added in 5.1.0 in the Switch tray icon.\
It allows you to reboot into your existing hekate boot entries with 2-3 taps.

### Widevine Streaming <a href="#widevine-streaming" id="widevine-streaming"></a>

Widevine L3 is supported for Streaming Movie and TV series platforms.

* For most platforms you can use `Firefox` ([from the firefox PPA on Ubuntu Jammy/Noble](https://launchpad.net/~mozillateam/+archive/ubuntu/ppa)) and the preinstalled `Chromium Web Browser` application.
* For others like Netflix, you can use `Firefox` ([from the firefox PPA on Ubuntu Jammy/Noble](https://launchpad.net/~mozillateam/+archive/ubuntu/ppa) with a [ChromeOS useragent](https://addons.mozilla.org/en-US/firefox/addon/user-agent-string-switcher/)) and the `Chromium Streaming` application.

***

## CPU and GPU Overclocking <a href="#nvidia-power-profile-applet-nvpmodel" id="nvidia-power-profile-applet-nvpmodel"></a>

### Power Profiles (all models)

Multiple custom CPU/GPU power profiles are included with switchroot linux.\
These are selectable by the Profile name at the taskbar near the Switch Icon.

Current L4T releases include these profiles by default:

**Switch 2017 (v1):**

```
Profile Name:
CPU/GPU: Min MHz - Max MHz

Console:
CPU: 0 - 1785
GPU: 0 -  768

Handheld:
CPU: 0 - 1428
GPU: 0 -  460

OC CPU:
CPU: 0 - 2091
GPU: 0 -  768

OC GPU:
CPU: 0 - 1785
GPU: 0 -  921

OC All:
CPU: 0 - 2091
GPU: 0 -  921

Perf All:
CPU: 1020 - 1785
GPU:  384 -  768

Perf OC All:
CPU: 1020 - 2091
GPU:  384 -  921
```

**Switch 2019 (v2) / Lite / OLED:**

```
Profile Name:
CPU/GPU: Minimum Frequency (MHz) - Max Frequency (MHz)

Console:
CPU: 0 - 1963
GPU: 0 -  844 (Lite: 768 or 844)

Handheld:
CPU: 0 - 1581
GPU: 0 -  537

OC CPU:
CPU: 0 - 2295 or 2397 (Lite: 1963 or 2091)
GPU: 0 -  844         (Lite:  768 or  844)

OC GPU:
CPU: 0 - 1963
GPU: 0 - up to 1228 (Lite: 768 or 844)

OC All:
CPU: 0 - 2295 or 2397 (Lite: 1963 or 2091)
GPU: 0 - up to 1228   (Lite:  768 or  844)

Perf All:
CPU: 1224 - 1963
GPU:  384 -  844 (Lite: 768 or 844)

Perf OC All:
CPU: 1224 - 2295 or 2397 (Lite: 1963 or 2091)
GPU:  384 - up to 1228   (Lite:  768 or  844)
```

More configs can be added by following the examples.\
Documentation for nvpmodel config can be found at the [link](https://docs.nvidia.com/jetson/archives/l4t-archived/l4t-3231/index.html#page/Tegra%2520Linux%2520Driver%2520Package%2520Development%2520Guide%2Fpower_management_nano.html%23wwpID0E0WK0HA).\
The nvpmodel config can be found here by default at\
`/etc/nvpmodel/nvpmodel_t210.conf` for Erista and\
`/etc/nvpmodel/nvpmodel_t210b01.conf` for Mariko devices.

### Boot Configuration OC Options (**Switch 2019 (v2) / Lite / OLED)** <a href="#ram_oc0" id="ram_oc0"></a>

#### dvfsb=0 <a href="#ram_oc0" id="ram_oc0"></a>

Controls DVFS B-Side for CPU/GPU/Core. Set 1 to enable.\
Reduces power draw in order to use less battery for the same performance.\
Can also allow higher CPU/GPU clocks. If OC is used, the reduced power draw\
is negated.

#### gpu\_dvfsc=0 <a href="#ram_oc0" id="ram_oc0"></a>

Controls DVFS C-Side for GPU. Set 1 to enable.\
Reduces power draw drastically on GPU frequencies of 768/844 MHz and up.\
Allows up to 1228 MHz clocks on select speedo binnings.

#### limit\_gpu\_clk=0

Sets GPU clock hard limit to 1075 MHz. Set 1 to enable.\
Helps when `gpu_dvfsc` is enabled and GPU can't handle the higher frequencies\
in such low voltages.

***

## RAM Overclocking <a href="#ram-overclocking" id="ram-overclocking"></a>

Max frequencies allowed are 2366MHz for T210 and 3200 MHz for T210B01.\
Can be set in ini.

RAM is automatically scaled per bw needs.

### ram\_oc=0 <a href="#ram_oc0" id="ram_oc0"></a>

Set RAM Overclock frequency in KHz.

If you hang or get corruption or artifacts, try to reduce it.\
Unlike HOS which uses encryption for storage, such situations are safer when\
they are happening in L4T, so just set power profile to handheld and reboot.\
`mem-bench` command can use almost all of the available bandwidth so it can be\
used for a quick testing.\
Actual stability can only be confirmed by `memtester` command on as high as\
possible RAM temperature for several hours and with a big test size.\
Running 2-4 instances of 128-256MB can help on running it on all RAM banks.\
Running a secondary GPU benchmark can help raise temperature.

Creating more L4T boot entries in the ini with different RAM clocks can help\
with fast testing. Especially when Reboot 2 Config from Switch tray icon is used.

Any clock increase will also increase RAM power consumption like a CPU/GPU OC.\
For example on T210B01, with 1z-nm ram, going from 1866 to 2133 can cause a\
146 mW increase on active reads/writes which is 19.8%.\
Any other state is affected only with a voltage change.

**Warning (T210B01):**\
On T210B01, high RAM frequency might require the GPU minimum voltage to\
increase. That depends on GPU Speedo binning.\
Since RAM OC can raise GPU voltages, again depends on GPU binning,\
consideration must be given based on needed workloads.\
The DVFS that manages that is dynamic. On idle or if ram bandwidth requirement\
and frequency drops, it switches back to stock minimum. That allows for more\
fine tuned power draws, without the need to have GPU min voltage raised\
all the time.

**T210 (Erista max 2366 MHz):**\
List of supported frequencies:\
1600000, 1733000, 1800000, 1866000, 1900000, 1933000, 1966000, 2000000,\
2033000,2066000, 2100000, 2133000, 2166000, 2200000, 2233000, 2266000,\
2300000, 2333000, 2366000.

Input frequency is generally normalized to one of the above.\
Actual frequency will differ a bit.

Suggested Jedec Frequencies:

* 1866000
* 2133000

Suggested Custom:

* Any, if it works.

**T210B01 (Mariko max 3200 MHz):**\
List of supported frequencies:\
1600000, 1866000, 2133000, 2166000, 2200000, 2233000, 2266000, 2300000,\
2333000, 2366000, 2400000, 2433000, 2466000, 2500000, 2533000, 2566000,\
2600000, 2633000, 2666000, 2700000, 2733000, 2766000, 2800000, 2833000,\
2866000, 2900000, 2933000, 2966000, 3000000, 3033000, 3066000, 3100000,\
3133000, 3166000, 3200000.

Input frequency is generally normalized to one of the above.\
Actual frequency will be exactly one of these.

Suggested Jedec Frequencies:

* 1866000
* 2133000

Suggested Custom Frequencies:

* Any, if it works.\
  Very high ram frequencies might raise GPU power draw on some GPU frequencies\
  if GPU binning is low.

**Timing based overclocking:**\
To enable that, edit the last **3 digits** of the frequency.

It's generally better to find a good base frequency before touching these.

Do not touch them at all for guaranteed stability on max possible frequency,\
since without these, the whole configuration is exactly per Nvidia's and\
RAM vendor's specifications.

Syntax:\
Freq MHz + CBA. FFFF**CBA**. (2133**000** -> 2133**252**)

Description of F, A, B, C timing overclocking options:\
&#xNAN;**\[FFFF]: RAM clock frequency**.\
Exceeding chip's real max is actual OC.

* Range: check above for T210 and T210B01 lists.

**\[A]: Base Latency reduction**.\
Base latency decreases based on selected frequency bracket.\
Brackets: 1066/1333/1600/1866/2133.

* Range: 0 - 3. 0 to -3 bracket change.\
  Example 1: 1866 with 2 is 1333 base latency. Originally 1866 bracket.\
  Example 1: 1866 with 3 is 1033 base latency. Originally 1866 bracket.\
  Example 2: 1996 with 3 is 1333 base latency. Originally 2133 bracket.\
  Example 3: 2133 with 2 is 1600 base latency. Originally 2133 bracket.\
  Example 4: 2400 with 0 is 2133 base latency. Originally 2133 bracket.

**\[B]: Core Timings reduction**.\
Timings that massively get affected by temperatures are not touched.

* Range: 0 - 9. 0% to 31% reduction.

**\[C]: BW Increase. RAM Temperature limited timings.**\
Can cause significant ram data corruption if ram temperature exceeds max.\
Reason it's not allowed at max on T210 with LPDDR4.

* 0/1/2/3/4: for max 85/75/65/55/45 oC RAM temperature

RAM Temperature is only related to MEM/PLL sensors and not to Tdiode or Tboard.\
T210B01 Temps:\
45oC is 50/51 oC MEM/PLL (around 43 oC Tdiode, depends on temp equilibrium).\
65oC is 68/69 oC MEM/PLL (around 60 oC Tdiode, depends on temp equilibrium).

**Full Examples:**

* 2200**000** -> 2200**000**: 2200 MHz with read/write base latency of 2133 MHz\
  and proper 2200 MHz core timings.
* 2200**000** -> 2200**252**: 2200 MHz with read/write base latency of 1600 MHz,\
  reduced core timings by 20% and up to 65C operation.

### ram\_oc\_opt=0 <a href="#ram_oc_vdd20" id="ram_oc_vdd20"></a>

Advanced timing reduction for tRP/tRAS/tRCD/tRTW/tWTR/tRFC/tRRD/tFAW.

Do not use if you don't know what you are doing.\
This allows to select specific reductions on specific timings.\
Check the [L4T RAM OC Configuration generator](linux-features.md#ram-oc-opt-configuration-generator) for more.

Enabling that, will disable the Core Timing reduction on all mentioned timings.\
Core Timing reduction is still applied on all other timings.

### ram\_oc\_vdd2=0 <a href="#ram_oc_vdd20" id="ram_oc_vdd20"></a>

Changes VDDIO/VDDQ voltage for T210. VDDIO only for T210B01.\
Can stabilize timing reduction or if at frequency limit.\
Limits up to 1175 are fully safe (official Jedec).

* Range: 1050 - 1175. For T210B01 (Unit in mV).
* Range: 1050 - 1237. For T210 (Unit in mV).

### ram\_oc\_vddq=0 <a href="#ram_oc_vddq0" id="ram_oc_vddq0"></a>

Changes VDDQ voltage for T210B01.\
Can stabilize timing reduction or if at frequency limit.\
Limits are fully safe (official Jedec).

* Range: 550 - 650. (Unit in mV).

### RAM OC OPT Configuration generator

This is a helper site that generates the correct config based on your parameters:

{% embed url="https://ctcaer.com/l4t/ram_oc.html" %}
