---
description: Installation guide/tutorial for Linux4Switch L4T Fedora on Nintendo Switch
---

# L4T Fedora 41 Install Guide

**Current version: Fedora 41  | L4S 5.1.2**\
Note: [hekate](https://github.com/CTCaer/hekate/releases/latest) 6.0.6 or newer MUST be used for this release.\
Maintainer: [azkali](https://github.com/Azkali) - [https://www.patreon.com/azkali](https://www.patreon.com/azkali)

If you need help you can ask in our [Discord Server](https://discord.gg/53mtKYt)\
You should also take a look at our [FAQ](faq.md) and [Common Issues](common-issues.md) which may already answer to some of your questions

### <mark style="color:red;">IMPORTANT: DO NOT UPGRADE TO FEDORA 42 - WE DO NOT SUPPORT DISTRO UPGRADE AS THEY BREAK OUR CONFIGS.</mark>

### Prerequisites <a href="#prerequisites" id="prerequisites"></a>

* [hekate](https://github.com/CTCaer/hekate/releases/latest) 6.0.6 or newer
* A 16GB SD Card MINIMUM (Recommended: 128GB and up U3 or U3/A2 class)
* A computer (for backing up and extracting the installation files)
* Latest [L4T Fedora rootfs](https://download.switchroot.org/fedora-41/)

### Installation <a href="#installation" id="installation"></a>

1. Follow our L4T Ubuntu 18.04 [install guide](l4t-ubuntu-bionic-installation-guide.md) and stop **before the last step** (backup restoration)
2. During L4T Fedora initial setup you **need** to create a user account.
   * Use + button on your Joycon to trigger the on-screen-keyboard
   * It is mandatory to boot and a root account is not sufficiant.
   * You may additionally setup a network connection and your timezone.
   * The initial setup installer can seem to hang but let it exit gracefully by simply waiting long enough (up to 30 seconds approximately)
3. Make sure any existing online updates are done via KDE`Discover` application or `dnf` command.\
   You can now proceed to restore your backup as mentioned in the last step of the ubuntu guide.
