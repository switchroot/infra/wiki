---
description: Here you can find all hekate boot entry ini keys supported by Linux.
---

# Linux Boot Configuration

## L4T .ini Configuration <a href="#l4t-ini-configuration" id="l4t-ini-configuration"></a>

**Warning:** Never use # to comment out keys in the ini.\
Either avoid them or use ; in front to invalidate a key.\
Example: `rootdev=mmcblk0p2` -> `;rootdev=mmcblk0p2`\
Also, never have an empty line between keys.

Below you can find all supported keys and their defaults.

### .ini Boot Entry Config Keys <a href="#ini-boot-entry-config-keys" id="ini-boot-entry-config-keys"></a>

* `l4t=1`\
  **1**: Enables and parses the .ini boot entry with L4T Loader.\
  Mandatory!
* `boot_prefixes=/switchroot/ubuntu/`\
  Sets the main L4T boot directory.\
  Mandatory!
* `emmc=0`\
  **1**: Enable eMMC.\
  Also allows booting from eMMC.
* `id=SWR-UBU`\
  Set FS Label name. SWR-UBU or something else by default.\
  Used to automatically find the correct linux partition.\
  Mandatory if `rootdev` is not defined!
* `rootdev=mmcblk0p2`\
  Default disk and linux partition.\
  Gets overridden via `id` if FS label is found.\
  Mandatory if `id` is not defined!\
  (sda1, sda2, sdb1, mmcblk0p2, mmcblk1p1, etc)
* `rootfstype=ext4`\
  Sets linux partition FS type.\
  Only ext4 is generally suggested since other filesystems use more CPU or\
  they are slower.
* `rootfs_fw=/lib/firmware/`\
  Sets firmware directory for kernel to search in.
* `rootlabel_retries=1`\
  How many times to retry and search rootdev.\
  Each iteration is 200ms. Useful when booting via USB (use 50 for 10s max polling).
* `auto_rootdev_disable`\
  Disable `rootdev` search done by initramfs which uses `id`.

***

* `r2p_action=self`\
  **self**: Reboots to self.\
  **bootloader**: Reboots to bootloader menu.\
  **normal**: Reboots with no config. Allows default auto boot to be used.
* `usb3_enable=0`\
  **1**: Enable USB3 support.\
  Enabling it can decrease WiFi 2.4GHz and Bluetooth signal quality.
* `4k60_disable=0`\
  **1**: Disable 4K@60 for Nintendo Switch (OLED).\
  If \[usb3\_enable] is set and 4K@60 is not disabled,\
  the driver will automatically choose between USB3\
  and 4K@60. 4K@60 is preferred in that case.
* `uart_port=0`\
  **0**: Disable serial logging\
  **1**: Enable serial logging on UART-A (internal testpads)\
  **2**: Enable serial logging on UART-B (right joycon rail)\
  **3**: Enable serial logging on UART-C (left joycon rail)\
  **4**: Enable serial logging on USB
* `earlycon=0`\
  **1**: If UART logging is enabled, this enables early boot logging.\
  If disabled, the backlog will be printed after uart driver init.
* `fbconsole=9`\
  **0**: Enable kernel logging on built-in display.\
  **1**: Enable kernel logging on DP/HDMI/VGA.\
  **9**: Disables kernel logging on any display.
* `als_enable=0`\
  **1**: Enable Ambient Light Sensor.
* `jc_rail_disable=0`\
  **1**: Disable railed Joycon support.
* `cec_disable=0`\
  **1**: Disable auto DP/HDMI CEC negotiation.
* `touch_skip_tuning=0`\
  **1**: Disables touch panel tuning on boot.\
  Some panels with broken flex cable might need it.
* `bt_ertm_disable=0`\
  **1**: Disable Bluetooth Enhanced Retransmission mode.\
  Might help with some Xbox Bluetooth based controllers.
* `wifi_disable_vht80=0`\
  **1**: Disable Wi-Fi VHT80 and VHT160 bonding (5GHz band).\
  In case wifi card firmware hangs when fully used at such speeds and kernel\
  panics, that might help to mitigate that issue.
* `bootargs_extra=`\
  Set extra kernel command line arguments.

### Overclocking <a href="#overclocking" id="overclocking"></a>

* `dvfsb=0`\
  **1**: Enable DVFS B-Side for CPU/GPU/Core. T210B01 only.\
  Reduces power draw in order to use less battery for the same performance.\
  Can also allow higher CPU/GPU clocks. If OC is used, the reduced power draw\
  is negated.
* `gpu_dvfsc=0`\
  **1**: Enable DVFS C-Side for GPU. T210B01 only.\
  Reduces power draw drastically on GPU frequencies of 768/844 MHz and up.\
  Allows up to 1228 MHz clocks on select speedo binnings.
* `limit_gpu_clk=0`\
  **1**: Set GPU clock hard limit to 1075 MHz. T210B01 only.\
  Helps when `gpu_dvfsc` is enabled and GPU can't handle the higher frequencies\
  in such low voltages.
* `ram_oc=0`\
  `ram_oc_opt=0`\
  `ram_oc_vdd2=0`\
  `ram_oc_vddq=0`\
  Set RAM Overclock configuration.\
  Check [RAM Overclocking Info](linux-features.md#ram-overclocking) for more.
