---
description: Installation guide/tutorial for L4T Lakka on Nintendo Switch
---

# L4T Lakka Install/Update Guide

**Current version:** [5.0-beta12-99d4f27](https://nightly.builds.lakka.tv/members/gavin/lakka-v5.x-new/Lakka-Switch.aarch64-5.0-devel-20231023052058-99d4f27.tar) (2023-10-23)\
Note: [**hekate**](https://github.com/CTCaer/hekate/releases/latest) **6.0.6 or newer MUST** be used for this release.\
Maintainer: [gavin\_darkglider](https://github.com/GavinDarkglider) - [https://paypal.me/gavindarkglider](https://paypal.me/gavindarkglider)

If you need help you can ask in [RetroNX Discord Server](https://discord.gg/J34Pxfz4Qf)

### Updating existing installations <a href="#updating-existing-installations-3xx5xx" id="updating-existing-installations-3xx5xx"></a>

If you come from a previous release and need to update.

* Go to `Main menu` -> `Online Updates` -> `Lakka updates` to get the latest version and then reboot to apply it.\
  **Note**: That method works since the 2023-10-23 5.0-beta12. If older, do a manually update.
* Alternatively, get the latest version from [https://nightly.builds.lakka.tv/members/gavin/lakka-v5.x-new/](https://nightly.builds.lakka.tv/members/gavin/lakka-v5.x-new/) manually and copy it **as is** in `sd:/lakka/storage/.update/` folder and then boot.\
  **Warning**: Do not rename the file!

### Prerequisites <a href="#prerequisites" id="prerequisites"></a>

* [**hekate**](https://github.com/CTCaer/hekate/releases/latest) **6.0.6 or newer**
* A 2GB SD Card **MINIMUM** with a **FAT32** partition in it.

### First Time Installation <a href="#installation" id="installation"></a>

1. Download the latest either from the version link at top or from here [here](https://nightly.builds.lakka.tv/members/gavin/lakka-v5.x-new/).
2. Open the .tar archive and navigate inside the folder you see that has the same name with the .tar. You will see `bootloader` and `lakka` listed.
3. Extract these 2 folders in your sd card root.
4. Optionally configure bootloader ini file. Info found in lakka/boot/readme\_config.txt
5. Boot via `hekate` -> `More Configs` -> `Lakka`

### Hints

You will have better performance using the vulkan video driver in most cores.
