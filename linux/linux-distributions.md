---
description: Understanding and setting up L4S distros
---

# L4T Linux Distributions

Various L4T-based linux distributions are available for the Nintendo Switch.

<table><thead><tr><th width="104">Distro</th><th width="92">Version</th><th width="192">Official Release Date</th><th width="126">Official EOL</th><th>Maintainer</th></tr></thead><tbody><tr><td><a href="l4t-ubuntu-bionic-installation-guide.md">Ubuntu Bionic</a></td><td>18.04</td><td>April 2018</td><td>ELTS: 4/2028</td><td><a href="https://github.com/CTCaer">CTCaer</a></td></tr><tr><td><a href="l4t-ubuntu-jammy-installation-guide.md">Ubuntu Jammy</a></td><td>22.04</td><td>April 2022</td><td>ELTS: 4/2032</td><td><a href="https://github.com/theofficialgman">theofficialgman</a></td></tr><tr><td><a href="l4t-ubuntu-noble-installation-guide.md">Ubuntu Noble</a></td><td>24.04</td><td>April 2024</td><td>ELTS: 4/2034</td><td><a href="https://github.com/theofficialgman">theofficialgman</a></td></tr><tr><td><a href="l4t-fedora-installation-guide.md">Fedora</a></td><td>39</td><td>November 2023</td><td>STS: 11/2024</td><td><a href="https://github.com/Azkali">azkali</a></td></tr><tr><td><a href="l4t-fedora-installation-guide-1.md">Fedora</a></td><td>41</td><td>October 2024</td><td>STS: 11/2025</td><td><a href="https://github.com/Azkali">azkali</a></td></tr><tr><td><a href="l4t-lakka-install-update-guide.md">Lakka</a></td><td>5.0</td><td>April 2024</td><td></td><td><a href="https://github.com/GavinDarkglider">gavin_darkglider</a></td></tr></tbody></table>

### Setting up your SD Card <a href="#setting-up-your-sd-card" id="setting-up-your-sd-card"></a>

1. Insert SD card to your PC
2. Download [TegraRcmGUI](https://github.com/eliboa/TegraRcmGUI/releases/) for Windows and run "TegraRcmGUI\_vX.X\_installer.msi" to install TegraRCMGUI (follow on-screen instructions).
3. Launch "TegraRCMGUI" and Install The APX driver if needed. This can be located in "Setting" -> "Install Driver"
4. Download [Hekate](https://github.com/CTCaer/hekate/releases) latest release and extract "heakte\_ctcaer\_X.X.bin"
5. Connect your switch to your PC/device via usb cable.
6. First, power off your Switch then insert the RCM Jig in the right joycon rail and press/hold VOLUME UP + POWER BUTTON for three seconds to enter into RCM
7. Find and select "heakte\_ctcaer\_X.X.bin" under "Select payload:" and hit "Inject payload". Then your Switch should now be in Hekate bootloader.

### Flashing a distribution for the first time <a href="#flashing-a-distribution-for-the-first-time" id="flashing-a-distribution-for-the-first-time"></a>

First time Linux installation

1. Partition the sd card in order to create an ext4 partition.\
   hekate's partition manager can be used for that: Tools -> Partition SD Card.
   1. The process in hekate is destructive, so you need to backup your fat partition files (if they are more than 1GB total) or emuMMC.
   2. _Move the sliders_ as you want them and then hit next. You will have plenty of chances/warns to backup your stuff if it's needed.
2. Make sure your Switch is connected via usb to your PC/Device and Select SD UMS
3. _Extract_ your distro of choice 7z directly to SD
4. Safely unmount and flash via hekate's _Flash Linux_
5. Then go to Home and Nyx options and dump your joycon BT pairing
6. Return to hekate's Home menu and choose "More Configs"
7. Boot

### Distributions layout <a href="#distributions-layout" id="distributions-layout"></a>

```txt
bootloader
|- ini
|  |- L4T-XXXXXXX.ini
switchroot
|- install
|   |- l4t.00        (4092 MiB parts. Last part can be 4 MiB aligned)
|   |- l4t.01
|   |- ...
|   |- l4t.XX
|- ubuntu
     |- boot.scr
     |- bl31.bin
     |- bl33.bin
     |- uImage
     |- initramfs
     |- nx-plat.dtimg
```
