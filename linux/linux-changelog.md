# Linux Changelog

## L4T Ubuntu Bionic Changelog: <a href="#l4t-ubuntu-changelog" id="l4t-ubuntu-changelog"></a>

<details>

<summary>Ubuntu Bionic 5.1.2 Full Changelog</summary>

### L4T Ubuntu 5.1.2 Changelog <a href="#l4t-ubuntu-511-changelog" id="l4t-ubuntu-511-changelog"></a>

#### Kernel/Driver changes <a href="#kerneldriver-changes" id="kerneldriver-changes"></a>

* RAM for T210B01 is now actually allowed to reach max possible frequency\
  In order for that to happen, DVFS was reworked to allow changes to GPU minimum voltage on the fly.\
  If RAM frequency changes to one that requires more voltage, depends on GPU binning, it will now automatically get raised.\
  That's dynamic and on idle or if ram bandwidth requirement and frequency drops, it switches back to stock minimum.\
  That allows for more fine tuned power draws, without the need to have GPU min voltage raised all the time.\
  Since RAM OC can raise GPU voltages, again, depends on GPU binning, consideration must be given based on needed workloads.\
  All previous test results on max ram frequency are now obsolete.
* The memory PTSA driver's calculations were "reworked"\
  That allows efficient use of ram when frequencies exceed 2133 MHz and especially when they exceed 2833 MHz\
  Driver rework gets enabled automatically depending on bootloader version.\
  On T210, that feature is disabled. Frequencies of > 2133 will just use the old method (exceeding 1.5 ratio).
* T210B01 RAM max frequency allowed is now 3200 MHz (raised from 3000)
* T210B RAM max frequency allowed is now 2366 MHz (raised from 2133)
* A critical issue was fixed that was disallowing a great number of Bluetooth devices to pair
* Fixed a rare bug that would cause a race condition and would stop the booting process
* Fixed all Nvidia bugs on T210B01's True RNG HW driver and a new driver was added to call it, so it can be used for rng seeding properly
* Joycon railed driver now allows more clones to work (for example non-bt clones that do not follow the proper procedure)
* An issue was fixed where the IMU input reports of Switch Lite gamepad would fail to be parsed if IMU chip was Invensense
* Fixed an issue where kernel would try to use CEC on docks with incompatible DP CEC emulation chip
* Added USB Serial driver for Silicon Labs CP210x

#### Ubuntu/Userspace changes <a href="#ubuntuuserspace-changes" id="ubuntuuserspace-changes"></a>

* Switch Multimedia package (`ffmpeg-l4t`, `ffplay-l4t`, `mpv-l4t`) was updated to allow proper fallback to software codecs.\
  If pixel format of the medium is provided via probing, the hardware codec will check if it is out of spec (e.g x264/x265 instead of h264/hevc) and will use the software decoding for it.\
  Additionally, lua/json scripting and osd-gui were enabled for `mpv-l4t`
* System FFMpeg was updated with the same changes also
* `mem-bench` was updated to provide info on the selected timing reducing config. cfg: ### will show near the frequency.
* Added `gpu-info-l4t` command that reports an extensive info about the gpu included in the SoC
* Joystick to mouse/keyboard configuration was updated to properly reflect xpad-neo's mapping for Wireless Xbox controllers

#### Boot Stack changes <a href="#boot-stack-changes" id="boot-stack-changes"></a>

* Initramfs
  * Now if Joycon BT pairing is redumped and Long Term Key (LTK) was changed, the bluetooth database gets updated with it.\
    No need to remove the pairing or pairing files and reboot like before.
  * Improved Volume button combo detection
* INI config
  * `ram_oc_vdd2`, `ram_oc_vddq` now allow undervolting. Check wiki or config readme for minimum values.
* Misc
  * Fixed an issue where 2397 MHz CPU frequency would not set
  * UBoot-L4T was updated to latest

</details>

<details>

<summary>Ubuntu Bionic 5.1.1 Full Changelog</summary>

### L4T Ubuntu 5.1.1 Changelog <a href="#l4t-ubuntu-511-changelog" id="l4t-ubuntu-511-changelog"></a>

**hekate `6.0.5 v2` or `newer` is mandatory** for doing the necessary configuration to the bootstack and kernel.\
Otherwise various features won't work correctly.

#### Kernel/Driver changes <a href="#kerneldriver-changes" id="kerneldriver-changes"></a>

* **RAM OC support of 2133 for T210 and 3000 MHz for T210B01**
* **Improved calibration support for Lite Gamepad (Sio).** **Needs hekate v6.0.5 v2 or newer**. Use the `Dump Joy-Con BT`.
* Fixed an issue where some Taishin/Silicon Power SD cards where they would fail to init
* Several extra checks were added for SD Card UHS DDR200 tuning.\
  Any card that requires the Sandisk method of tuning and fails these checks is barred from using it and they fallback to UHS SDR104.\
  `ddr200_disable` is now removed, since it was abused and no one reported the issue to the proper channels.
* Stick Calibration is now tighter for Joy-Con/Pro-Con/Lite controllers when factory or user calibration is empty or can't be acquired.
* Fixed an old railed Joy-Con issue which could make UART communications hang if it was plugged during suspend procedure.
* Added Generic and FTDI USB serial drivers as modules
* Support unfused BRCM4354 mascaraed as BRCM4356 WiFi chip on some old Switch units
* Add support for WiFi 5 GHz bonding restriction of VHT80 and VHT160 to mitigate possible bugs in the WiFi firmware if needed.

#### Ubuntu/Userspace changes <a href="#ubuntuuserspace-changes" id="ubuntuuserspace-changes"></a>

* `mem-bench` command was added. It is able to use most of RAM bandwidth. Use together with Perf OC All power profile for better results.\
  Frequency reported there is the actual real frequency of RAM. Reason why it changes everytime, because of clock jitter.
* Fixed an issue with `jc-dump-ltk` command failing to acquire Pro-Con or Joy-Con Grip MAC addresses and LTK properly.

#### Boot Stack changes <a href="#boot-stack-changes" id="boot-stack-changes"></a>

*   **Initramfs**\
    \-- Failed to find rootfs issues can now also be debugged via USB. Simply follow the instruction in the Red screen and connect to a PC.\
    \-- Added support for Procon auto BT pairing creation\
    Simply add this in `switchroot/joycon_mac.ini`

    ```
    [joycon_02]
    type=3
    mac={MAC address of Procon}
    host={MAC address of Switch}
    ltk={LTK (connection key) of Procon}
    ```

    Copy

    And fill in the {brackets} with the correct information. All of it can be acquired from the `jc-dump-ltk` command when you connect Procon via USB.\
    Reminder that if the file is created anew, the line-endings MUST be Unix based. CRLF are not supported.
* **INI config**\
  \-- **Added `ram_oc_vdd2`, `ram_oc_vddq` for fine tuning RAM voltage and improved `ram_oc` key. Read below for more.**\
  \-- Added `wifi_disable_vht80` key that can disable WiFi 5GHz bonding of VHT80 and VHT160 to mitigate possible bugs in the WiFi firmware if needed.\
  All info can be found in `README_CONFIG.txt` also.
* **Misc**\
  \-- **Added full proper factory calibration support for Lite Gamepad.**\
  Get latest hekate, **v6.0.5 v2 or newer**, and use `Dump Joy-Con BT` to enable it.

For RAM OC help, check [RAM Overclocking](linux-features.md#ram-overclocking).

</details>

<details>

<summary>Ubuntu Bionic 5.1.0 Full Changelog</summary>

### L4T Ubuntu 5.1.0 (r3) Changelog <a href="#l4t-ubuntu-510-r3-changelog" id="l4t-ubuntu-510-r3-changelog"></a>

**hekate `6.0.3` or `newer` is mandatory**

#### r2/r3 changes <a href="#r2r3-changes" id="r2r3-changes"></a>

Kernel/Driver:

* Several extra checks were added for SD Card UHS DDR200 tuning.\
  Any card that requires the Sandisk method of tuning and fails these checks is barred from using it and they fallback to UHS SDR104.\
  `ddr200_disable` is now removed, since it was abused and no one reported the issue to the proper channels.
* Stick Calibration is now tighter for Joy-Con/Pro-Con/Lite controllers when factory or user calibration is empty or can't be acquired.
* Fixed an old railed Joy-Con issue which could make UART communications hang if it was plugged during suspend procedure.\
  Userspace:
* Fixed an issue with `jc-dump-ltk` command failing to acquire Pro-Con or Joy-Con Grip MAC addresses and LTK properly.

#### Kernel/Driver changes <a href="#kerneldriver-changes-1" id="kerneldriver-changes-1"></a>

* **SD/eMMC Dramatic Performance Increases**\
  Support for SD card **UHS DDR200** allowing for up to 200MB/s on UHS-I bus (up from 104MB/s). (Needs hekate 6.0.3).\
  That marks the first device in the world to even support this. (HW is 4 years older than DDR200 and doesn't provide official support).\
  Read performance increased by **2-20%** and write by **50-200%** on **SD and eMMC**. That's by fixing an almost 2 decades old bug.\
  Fixed an old Nvidia bug where it would decrease SD/eMMC performance by 60% after a deep sleep.\
  Decreased latency/access times and improved random transfers (4k IOPS)\
  Added support for SD card cache\
  SD Card VDDC/VDDQ regulators are now disabled on deep sleep. Increases standby times dramatically on chipped switch that use the proper SD card VDD point.\
  On failed inits, the SD card will now be power-cycled fully and driver will try to reduce bus timing progressively, instead of falling back to HS25 immediately. Finally, retries are now instant, instead of taking 2-5s.
* **Deep Sleep**\
  Significantly improved wake up speed on T210 and T210B01\
  Significantly improved deep sleep on T210B01 (increased by \~10-15 days)\
  Mitigated a T210B01 HW erratum and fixed several Nvidia driver bugs in SDMMC/PCIE/XUSB/USBPD drivers
* **GPU DVFS for T210B01**\
  A new mode added for T210B01 that dramatically lowers power consumption on higher GPU clocks.\
  That also allows GPU to reach 1228 MHz max on select binned GPUs.\
  GPU clocks are now properly limited by max voltage and max power, increasing safety by not exceeding pmic current limits.\
  That can also increase max GPU clock to 1152 MHz on select binned GPUs.
* **Joy-Con/Pro-Con**\
  Improved Sio (Lite Gamepad) init\
  Increased Sio input report rate to 120HZ (8ms) from 60Hz (16ms)\
  Reduced traffic from rumble commands\
  Added plug-in/removal detection via pins for railed Joy-Con\
  Fixed an issue where the IMU input device would not be unregistered on removal of railed or wireless Joy/Pro-Con
* **XBOX Controllers**\
  Added support for bluetooth XBOX controllers via XPAD Neo driver\
  As with USB ones, userspace must load the module.
* **WiFi/Bluetooth**\
  Updated firmware files to latest from HOS\
  Fixed several bluetooth driver bugs
* **USB-PD**\
  VCONN/VBUS is now managed on certain SKUs, increasing deep sleep dramatically.
* **PCIE/XUSB**\
  Improvements to the drivers in all fronts.\
  The Suspend/Resume fixes also increase deep sleep times dramatically.\
  Fixed an issue where USB on dock would not work if booted docked.
* **Built-in IMU**\
  Added support for STM LSM6DSO/LSM6DO and LSM6DSE 6-Axis IMU chips.\
  Now the driver will also automatically choose between LSM6DS3H/DSO/DSE and ICM40607
* Added HW Random engine support for T210B01
* Fixed production settings for DisplayPort on T210B01
* Fixed several pmic settings
* Many more improvements/fixes

#### Ubuntu/Userspace changes <a href="#ubuntuuserspace-changes-1" id="ubuntuuserspace-changes-1"></a>

* **Chromium Widevine Support for Movie/Series Streaming Services**\
  Chromium now supports Widevine DRM for streaming platforms. Most sites work via the normal icon.\
  Others, like Netflix, need the Chromium Streaming one from the app menu.
* **Switch Tray Icon**\
  The icon is now split into 2 sections. The Switch icon for tools and the Text profile one that allows Power/Fan management.\
  The following features were added to the icon:\
  Added Reboot 2 Config app for rebooting to a hekate boot entry with 2 simple steps\
  Added OLED display color modes\
  Added Joy-Con Controls Mapping help. Shows how Joy-Con are mapped to keyboard/mouse and how to enable/disable it.
* **Theme**\
  Unity Dash, the apps side bar, was updated to a modern flat theme.
* **Apps**\
  Added `tegrastats-l4t` with better formatted output. Useful for live info and overlays. Can be launched from the Switch tray icon.\
  Added `jc-dump-ltk` for dumping Bluetooth LTK key when user connects Pro Controller via USB. Can be also used for getting LTK from Joy-Con via Grip.
* **Bluetooth**\
  Bluetooth now gets disabled in Sleep, for not needing user to turn off controllers manually.\
  This also allows the whole wifi/bt combo chip to power down during sleep and thus decreasing power consumption.
* Optional support for DTS audio encoder (dcaenc) for DP/HDMI was added. If wanted, apt package is `alsa-lib-dcaenc`.
* Many more improvements/fixes

#### Boot Stack changes <a href="#boot-stack-changes-1" id="boot-stack-changes-1"></a>

* **L4T Loader**\
  SD Card UHS DDR200 mode support (needs hekate 6.0.3 or newer)\
  Allow early 2019 Samsung ram to be overclocked to 2133MHz for T210B01 (Mariko)
* **INI config**\
  `bt_ertm_disable` allows disabling Bluetooth ERTM which might hinder usage of some Bluetooth devices\
  `ddr200_disable` allows disabling the automatic SD Card UHS DDR200 support\
  `gpu_dvfsc` allows dramatically decreasing GPU power consumption for Mariko on high clocks and allowing for higher clocks also\
  `limit_gpu_clk` allows limiting max GPU clocks in case `gpu_dvfsc` is used and GPU can't withstand lower voltage\
  As always, check README\_CONFIG.txt for more info.
* Support for beefy pmic type of T210B01 devboards in U-BOOT

</details>

<details>

<summary>Ubuntu Bionic 5.0.0 Full Changelog</summary>

### L4T Ubuntu 5.0.0 Changelog <a href="#l4t-ubuntu-500-changelog" id="l4t-ubuntu-500-changelog"></a>

#### Kernel/Driver changes <a href="#kerneldriver-changes-2" id="kerneldriver-changes-2"></a>

* **RAM**\
  409MB RAM was freed from carveouts and was given to CMA so everything can easily use it with mmap
* **Joycon/Procon driver**\
  Added support for Sio (Switch Lite gamepad)\
  Improved HD Rumble and fix it once and for all\
  Improved and reorder init sequence\
  Improved high speed uart stability\
  Fixed hangs when using hd rumble\
  Changed Procon D-Pad from HAT to actual D-Pad\
  Fixed HORI analog stick limits
* **WiFi & Bluetooth**\
  Fully unlocked almost all WiFi channels,1-13 for 2.4GHz and 34-165 for 5GHz. 14/144 disabled).\
  Many 5Ghz channels will only be used if there's an existing AP (radar detection), for regulation reasons.\
  Fixed Bluetooth SCO mode (HSP/HFP). It now properly transmits audio instead of static\
  Significantly improved signal on both WiFi 2.4GHz and Bluetooth since default is now USB2\
  (USB3 can be enabled back via ini config)
* **Battery Charger and Fuel Gauge**\
  Added Battery % regulation. Sets a limit to battery max % and it's quite useful for when device is always docked.\
  Added support of charging disable via special GPIO\
  Improve init sequence
* **Audio**\
  Added Switch 2019/Lite/OLED speakers EQ\
  Added fine controls for DAC speaker/headphone volumes\
  Added minijack Mic support and Headset vs Headphones support\
  Added Headset button support (all buttons work as play/pause)\
  Refactored driver and improved init/deinit process
* **R2P**\
  Refactored driver and added T210B01 support\
  Check README\_CONFIG.txt for more info on how it can be configured
* **USB-C (DisplayPort/Power Delivery)**\
  Massively refactor USB-C driver, again.\
  Support for PIN C (adapters that report 4 DisplayPort lanes only)\
  Removed the requirement for at least 2 USB-PD profile to enable PD charging\
  Added support for no DisplayPort (Switch Lite)\
  Improve and make init sequence way faster
* **Display**\
  Added OLED panel driver\
  Added HDCP 1.x and 2.2 support for DisplayPort\
  Fixed TSEC support\
  Backlight will always get disabled before disabling LCD/OLED panel
* **DisplayPort CEC**\
  Added support for CEC via DisplayPort\
  Docking will change source to Switch automatically if supported\
  CEC tv remote control\
  Support for OLED dock
* **Non-PD USB charger detect**\
  Added fine tuners for currents on USB charger detect\
  Improve detection\
  Set SDP when a gadget is enabled to 500mA only
* **eMMC/SD driver**\
  Improve auto calibration\
  Allow 1-bit mode
* **XUSB**\
  Fixed T210B01 support\
  Fixed USB3 device mode\
  Added proper support for USB2-only mode\
  Various fixes to USB phy and vbus
* Improved kernel driver init sequence. Saves around 4-6s of boot time.
* Added better panic error handling, the relevant log can now be dumped automatically by hekate.
* Added controls for disabling touch panel tuning on boot (some broken flex cables or panels might need that)
* Fixed IMU IRQ mode. This greatly improves IMU Acc/Gyro responsiveness. Also added new IMU chips support.
* Fixed an issue where Power button irq could be stack
* Fixed a hang on boot because of Squash FS driver
* Added DM-CRYPT support
* Fixed max77812 CPU/GPU/RAM regulator and enable it for T210B01
* Added max77801 3.3V regulator for Switch OLED
* Added a new TSKIN driver and changed temperatures and fan curves to match HOS exactly

#### Ubuntu/Userspace changes <a href="#ubuntuuserspace-changes-2" id="ubuntuuserspace-changes-2"></a>

* **Theme and Icons**\
  Default theme and icons changed to more modern flat and dark one\
  If you changed them before, you can use `Tweaks` from app menu to change it (Pop-Dark theme and Pop icons)
* **On-Screen Keyboard**\
  Now rocks a new material you design look\
  Add special key for Gamepads to Show/Hide it\
  Enabled physical Keyboard detection\
  Auto transparency on idle, so user can see behind it.\
  Fixed an issue which would delay logout 2:30 minutes if osk is enabled
* **Default settings**\
  Double click is now easier to do on touch\
  Drag threshold increased to avoid miss-dragging instead of tapping\
  ALS disabled for backlight control so user can enable it in ini config for emulators and games\
  Changed Battery levels: Warns LOW at 15%, Warns Critical at 6%. Shutdowns at 4%\
  Suspend after 10min idle at battery. Never at AC.\
  Battery percentage always shown\
  Cursor size default increased\
  Default scaling changed\
  Default Dash shorcuts changed so user can pin preferences without first unpinning
* **Bluetooth A2DP profiles**\
  Automatic profile change\
  Added AAC, AptX and AptX HD profiles\
  Also fixed SCO HSP/HFP profiles
* **Fixed 5.1 surround audio**
* **Dock-Handler**\
  Fully optimized and fast\
  Saves boot time\
  Works properly on Greeter\
  CEC support\
  PD power profile support (Will switch to Console if PD charger)\
  Fixed crashes on boot
* **Joycon Daemon**\
  Added Sio (Switch Lite Gamepad) support\
  Now rumble works properly and also doesn't hang system\
  Removed Procon management from Joycond. Now the controller can be used by any software\
  IMU now accessible from every app\
  Various bug fixes
* **System FFMpeg** updated to use latest HW Video codecs
* **Switch Multimedia**\
  Updated to use the latest HW Video decoding/encoding\
  (ffmpeg-l4t, ffplay-l4t and mpv-l4t)\
  All video files now use SMPV Player by default for hw decoding
* **Chromium Browser**\
  Full Vulkan HW acceleration support\
  Full HW Video decoding support for H264 and VP8 videos
* Bluetooth is now disabled completely on sleep to save battery\
  Enabled automatically on resume.
* **Debloated and remade main image from scratch**
* **Joystick Mapping**\
  Based on L4T Megascript one and included by default\
  Reworked all mappings to the following:\
  A, B, X, Y --> Enter, Backspace, Esc, Task Switch\
  Minus, Plus --> Rotate Screen, Show/Hide OSK\
  L3, R3 --> Caps Lock, Middle Click\
  Home --> Windows Key\
  D-pad --> Arrow keys\
  L/R Sticks --> Mouse, Scrolling\
  Capture --> Disables(/Enables) mappings and allows it to be used as gamepad
* Now All updates can be done via APT. 5.0.0 will be the last image provided.\
  Every new update will be done through Software Updates or Apt command.
* Updated ALSA profile to support all SKUs
* Added HDCP support if requested by an App that supports the api

#### Initramfs changes <a href="#initramfs-changes" id="initramfs-changes"></a>

* WiFi MAC address is now derived from SoC id
* BT MAC address if no joycon dump is derived from SoC id
* Added error checking at boot\
  Fixes "bricks" where linux partition is corrupted but can be fixed
* Creating `.reinit` file flag in switchroot/ubuntu will force a re-extraction of Kernel and Modules/Firmware\
  That should always be used if user updates BSP to another one (for example r32.7.3)
* If initramfs fails booting it will try to dump kernel log in sd first (l4t\_initramfs\_kmesg.log) before showing the red screen.

#### Boot Stack changes <a href="#boot-stack-changes-2" id="boot-stack-changes-2"></a>

* L4T Loader - specialized bootloader
* Full support for T210B01 (Switch 2019, Lite and OLED)
* .ini file is now the main configuration file and uenv.txt is obsolete (a backup is done for existing copies)
* All configs are described in README\_CONFIG.txt
* Careful with what you are booting on T210B01 since it can create hangs in black screen or error messages.\
  Error message will be shown if that's the case, to avoid a black screen hang.

</details>

<details>

<summary>Ubuntu Bionic 3.4.0 Full Changelog</summary>

\- Better performances in many scenarios (As always that should affect apps/games/emus/etc)\
\- Significant better deep sleep. (Measured at 582 hours. 440% better).\
\- Faster wake up from sleep.\
\- Saved 240MB ram (fb and active file page pool, aka always used pool)\
\- Memory remapping 800%-2000% speed up. Mostly used by emulators and VMs (and lot in android).\
\- Some other memory optimizations, plus enabled huge ram pages\
\- Faster kernel and OS boot.\
\- Fixed tearing issues for NVDEC/NVENC. (video hw accel)\
\- Lower latency for frame end calcs for builtin display\
\- Fixed Hori pads for joycond\
\- Fixed an issue where it would make railed jc to disconnect on rumble\
\- Joycon/Procon rumble was improved and intensity tuned for better user experience\
\- Kernel now uses the proper display panel init/deinit by checking panel id\
\- Improved dock/undock script\
\- Touch stays enabled now in dock mode. Users that use docks that do not cover the screen can use it as alternative input.\
\- Added low ram protection. Now instead of hanging for minutes it will only stutter for seconds until oom occurs and recovers ram\
\- Better seamless display on boot\
\- uenv.txt changed, read uenv\_readme.txt. It's simple to transfer over custom settings.\
\- Reboot action can now be defined in new uenv.txt. Reboot back into Linux by changing \`reboot\_action=\` from \`bootloader\` to \`via-payload\`.

Apps Changes:

\- Full video HW decoding via new SMPV Player app (also supports youtube links)\
\- Full video HW encoding via ffmpeg-l4t\
\- Added [L4T-Megascript app](https://github.com/cobalt2727/L4T-Megascript/wiki)\
You can get or build various essential stuff, apps or emulators with a single click. All configured and optimized for Switch.\
\- Nvpmodel was updated to include Battery charging protection limits options\
Useful when you are constantly docked and want to protect battery's life.\
Limit gets saved and applied on boot.\
Reminder that discharging can't happen when plugged in. Even when charging is off, usb power is used to supplement system.\
So you need to discharge first if you want the battery % to go inside the limit.\
That also disables charging in sleep in order for the feature to work.\
If you reboot/shutdown, it gets disabled and re-enabled at the next L4T boot.\
As always the source is public and all L4T based projects for Switch will benefit (L4T linux distros/Lakka/Android)

</details>

<details>

<summary>Ubuntu Bionic 3.3.0 Full Changelog</summary>

\- Significant performance improvements\
CPU arch, RAM, GPU, general latency, etc optimizations.\
Affects gui, apps, games, emus, etc\
\- Significant deep sleep power draw fixes\
Now almost all non-needed devices power off on deep sleep and ram is set to appropriate frequency.\
(Touch panel, LCD, Joycon charging and many more devices now power off in deep sleep as they should)\
\- IO scheduler was changed to provide way faster storage IO\
\- Released 15 MiB extra ram\
\- Fixed voltage calculation for OC. It should provide more stable overclocking\
\- Refactored thermal management\
Fan profiles are now the same with HOS\
Added failsafes, throttling and force power off when very high temps for the board are reached\
\- Faster boot times\
\- Fixed issues with Joycon charging and Hori power on resume\
\- Fixed Power button issues\
\- Touch improvements\
\- Added Hori and Obirds rail support\
\- OTG now allows deep sleep, even if linux resides in a USB SSD.\
\- Various USB fixes\
\- Allow fuel gauge to wake up system and power off gracefully on empty battery\
\- Added support for many 3rd party docks\
These include Genki, Zenscreen, Nexdock and anything else that uses similar methods\
Basically, the support should reach 100% (docks that support DP via pin D)\
\- Fixed issues with fast charging when docked on boot\
\- Fixed other issues with charging on certain edge cases\
\- Fixed DP/HDMI when plugging console while sleeping\
\- Fixed DP/HDMI when already docked and waking up\
\- Dock led indicator now has a breathing light when sleeping\
\- Add exFAT partition support (boot driver still needs to be FAT32)\
\- Enabled 1GB ZRAM swap

\- Status messages on booting\
\- Seamless display booting (needs hekate higher than v5.5.4)\
\- Disabled screen blanking on idle. Dimming/Screensaver is still there and can be disabled in Brightness & Lock.\
This will help with games/emus where gamepad is used and it does not reset idle times like mouse/keyboard.\
\- Fixed apt repo urls

\- NVPModel is revamped\
Allow changing power profiles via touch\
Added handheld power profile\
Added fan profiles\
Added automatic profiles based on handheld or docked\
Added USB 3.0 disable toggle for boosting WiFi 2.4G and Bluetooth signal\
OC profiles now do not require enabling overclock flag in sysfs

</details>

<details>

<summary>Sources</summary>

[Switchroot Gitlab](https://gitlab.com/switchroot)\
[CTCaer L4T Github](https://github.com/CTCaer?tab=repositories\&q=l4t)

</details>

## L4T Fedora Changelog:

<details>

<summary>Fedora 41 5.1.2 Full Changelog</summary>

### Fedora 41 changelog

* Keep firefox which should now have hardware acceleration support (credits: [theofficialgman](https://app.gitbook.com/u/goHY87dk9OM4WhASz9CxsuzIjlg2 "mention")& [Azkali Manad](https://app.gitbook.com/u/9pDIgeehhjNw9lGFbSPSyB6k0ou2 "mention"))
* Improve FFMpeg and update to 7.0.2
* Fix all previously mentioned Onboard bugs
* Revert all Xorg components to 1.20 and/or compatible ABI&#x20;

### Known bugs

* USB serial doesn't work (uart\_port=4 in ini)

</details>

<details>

<summary>Fedora 39 5.1.2 Full Changelog</summary>

### Fedora 39 changelog

* Use pipewire instead of Pulseaudio and add full support for it
* Add 32.7.4 BSP for vulkan 1.2 and set it by default
* Reverted to fedora's Glibc as widevine patches are not needed anymore
* Update FFmpeg to 6.0.1
* Add Apps repo by default
* PR fix for hangover compilation
* Fix Vita3k build in apps repo
* Complete cleanup of OBS repos
* Add KDE spin and use it by default
* Created pipeline for CUDA
* Added CUDA 10.2
* Add GCC-7 for CUDA
* Fixed BSP override bug
* Improve dock-hotplug to use most common DM greeters when available
* Fix kernel src symlink for kmod
* Use nvidia container toolkit from new nvidia repo
* Enable USB\_SERIAL\_CH341 module
* Fix and use onboard
* Show onboard in sddm and initial-setup
* Fix nvpmodel fan\_available\_profile sysfs node check (credits [CTCaer](https://app.gitbook.com/u/eN6jtKe0KOUPJ8YEli3WQ7q3TJI3 "mention"))
* Decrease time spent in initramfs by uncluterring it
* Chromium is now weekly updated (credits [theofficialgman](https://app.gitbook.com/u/goHY87dk9OM4WhASz9CxsuzIjlg2 "mention"))

### Known bugs

* Onboard (the visual keyboard) can crash sometimes (recovered automatically by a service)
* Onboard layout except Compact bug on special characters such as ,.?!
* Onboard can crash in sddm (quick fix: reboot or enable autologin)
* USB serial doesn't work (uart\_port=4 in ini)

</details>

<details>

<summary>Sources</summary>

[L4S GitLab](https://gitlab.azka.li/l4t-community/gnu-linux/)\
[Switchroot Gitlab](https://gitlab.com/switchroot)\
[CTCaer L4T Github](https://github.com/CTCaer?tab=repositories\&q=l4t)

</details>
