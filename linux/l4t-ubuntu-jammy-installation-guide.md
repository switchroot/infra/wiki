---
description: Installation guide/tutorial for Switchroot L4T Ubuntu Jammy on Nintendo Switch
---

# L4T Ubuntu Jammy 22.04 Install Guide

**Current version: 5.1.2** based on Ubuntu Jammy Jellyfish 22.04 LTS\
Note: [**hekate**](https://github.com/CTCaer/hekate/releases/latest) **6.0.6 or newer MUST** be used for this release.\
Maintainer: [theofficialgman](https://github.com/theofficialgman) - [https://github.com/sponsors/theofficialgman](https://github.com/sponsors/theofficialgman)

If you need help you can ask in our [Discord Server](https://discord.gg/53mtKYt) under #jammy-support\
You should also take a look at our [FAQ](../faq.md) and [Common Issues](../common-issues.md) which may already answer to some of your questions

Note: The [latest LTS Release of Ubuntu (Noble 24.04) is now available](l4t-ubuntu-noble-installation-guide.md) instead of this older (Jammy 22.04) release.&#x20;

### Prerequisites <a href="#prerequisites" id="prerequisites"></a>

* [hekate](https://github.com/CTCaer/hekate/releases/latest) **6.0.6 or newer**
* A 16GB SD Card **MINIMUM** (Recommended: 128GB and up U3 or U3/A2 class, see [SD Card Guide](../sd-card-guide.md) for more details)
* A computer (for backing up and extracting the installation files)

### Installation <a href="#installation" id="installation"></a>

1. Download the latest version of the L4T Ubuntu Jammy Image directly or via torrent from [here](https://download.switchroot.org/ubuntu-jammy/).
2. Backup your SD Card:
   * Backup normal files from FAT32 (this also backs up emuMMC if file based)
   * If emuMMC raw partition exists, go to `hekate` -> `Tools` -> `Backup eMMC`.\
     Tap on `SD emuMMC RAW Partition` button and back it up (emuMMC BOOT0/1 and emuMMC Raw GPP)
   * If android exists, do a TWRP backup\
     \
     Suggestion: Use hekate UMS (`hekate` -> `Tools` -> `USB Tools`) with a USB-C cable connecting your switch and PC to mount your SD card and transfer files instead of swapping your card from Switch to PC.\

3. **WARNING: This following will wipe everything on your SD card.**\
   Partition your SD card in `hekate` -> `Tools` -> `Partition SD Card` leave at least 6GB for FAT32 for installation files downloaded in step 1 **AND** recreate any partition that may have backed up.

<figure><img src="../.gitbook/assets/nyx20890123_204922.png" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/nyx20890123_204928.png" alt=""><figcaption></figcaption></figure>

4\. From your PC, extract the **7z** to the root of your SD FAT32 partition (Note that Win11's builtin 7z extract support is broken, use 7-zip and make sure not to create a new folder on extract). ALWAYS SAFELY EJECT storage when finished or you WILL corrupt the installation files. Suggestion: Use hekate UMS (`hekate` -> `Tools` -> `USB Tools`) with a USB-C cable connecting your switch and PC to mount your SD card instead of swapping your card from Switch to PC.\
\
5\. In Hekate, go to `Tools` -> `Partition SD Card` -> `Flash Linux`.\
\
6\. Go to `hekate` -> `Nyx Options` -> `Dump Joy-Con BT` to dump Joycon pairing data, with both Joycon connected to console, after they were paired in hos first.\
This must be run even if a **Switch Lite** is used, since it dumps the factory calibration data.

<figure><img src="../.gitbook/assets/nyx20890123_202109.png" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/nyx20890123_204917.png" alt=""><figcaption></figcaption></figure>

7. Now you can boot L4T Ubuntu Jammy (under `More Configs`).\
   Make sure any existing online updates are done via `Software Updater`, `Software` (formerly known as Gnome Software Center), or `apt` command.
8. After making sure everything is fine, you can now restore your backed up files
   * Copy your normal files to FAT32
   * For emuMMC use Hekate to restore your backup **`hekate` -> `Tools` -> `Restore eMMC`**
   * For Android, copy your TWRP backup to your FAT32 partition then follow the Android setup guide starting at step 4 **BUT instead of installing "Lineage zip, followed by your GApps zip" at step 5.(f on 10, g on 11) restore your backup in TWRP**

### Updating <a href="#updating" id="updating"></a>

Updates are shipped OTA like any standard Ubuntu Distro, so use the `Software Updater` or `GNOME Software` application.\
Or open terminal and execute: `sudo apt update && sudo apt-get dist-upgrade`

### Next Steps <a href="#next-steps" id="next-steps"></a>

* [**Read about our Supported Features**](linux-features.md)
  * Flatpak support out of the box in Software (Gnome Software Center)
  * [JoyCon Mouse/Keyboard mapping](linux-features.md#joycon-mousekeyboard-mapping)
  * [Pairing Nintendo Switch Pro Controller](linux-features.md#joycon-and-switch-pro-controller-native-support)
  * [CPU/GPU Overclocking](linux-features.md#nvidia-power-profile-applet-nvpmodel)
  * [RAM Overclocking](linux-features.md#ram-overclocking)
* [**L4T-Megascript**](https://github.com/cobalt2727/L4T-Megascript/wiki/Initial-Setup#installrun-the-megascript)
  * Available in your apps list\
    You can get or build various essential stuff, apps or emulators with a single click. All configured and optimized for Switch.
* [**Pi-Apps**](https://pi-apps.io/)
  * Install from the L4T-Megascript or follow the [official install instructions](https://pi-apps.io/install/).\
    A well-maintained collection of nearly 200 app installation-scripts with a focus on desktop applications that you can run with one click.
* Touchscreen Gestures:
  * 3 finger swipe left/right to switch workspaces
  * 3 finger swipe up to bring up activities overview
  * drag up from bottom edge to bringup keyboard
  * drag down from top edge to exit fullscreen

### Missing Features <a href="#missing-features" id="missing-features"></a>

These features are currently missing in comparison to L4T Ubuntu 18.04 and may be available in future releases.

* No CUDA compiler support (CUDA runtime 10.0 is preinstalled and functions)
* No GSTREAMER HW Decode or Encode (eg: GNOME Totem Videos)
  * NOTE: FFMPEG based players DO HAVE HW Decode/Encode and work great (eg: MPV, SMPLAYER, and VLC)

### Additional Notes <a href="#additional-notes" id="additional-notes"></a>

* GNOME Software does not always work on first boot. If it seems to not load after a couple of minutes, reboot and try again and it should work from then onwards.
* Onscreen keyboard will attempt to move and resize windows when popping up so there is space for the keyboard. If this fails it will close immediately. Either manually resize the window so it is small enough for the keyboard to fit beneath it or use the application in maximized mode (where resizing always works).
* Onscreen keyboard will auto-hide on mouse movement (a non-configurable GNOME feature). If you have joycon drift (greater than 15% deviation from center point) then you can unintentionally trigger this and hide the keyboard when not physically moving the stick. If you have drift, then get your joycons repaired, clean our your stickboxes with compressed air, and/or calibrate your joycons in HOS.

### Sources

Linux kernel buildscripts: [https://github.com/theofficialgman/l4t-kernel-build-scripts](https://github.com/theofficialgman/l4t-kernel-build-scripts)

noble/jammy image buildscripts: [https://github.com/theofficialgman/l4t-image-buildscripts](https://github.com/theofficialgman/l4t-image-buildscripts)

theofficialgman debian repository: [https://github.com/theofficialgman/l4t-debs](https://github.com/theofficialgman/l4t-debs)

theofficialgman debian repository sources: [https://github.com/theofficialgman/l4t-debs-source](https://github.com/theofficialgman/l4t-debs-source)

tegra hw decode/encode ffmpeg: [https://github.com/theofficialgman/FFmpeg/tree/4.4.2-nvv4l2](https://github.com/theofficialgman/FFmpeg/tree/4.4.2-nvv4l2)

chromium tegra buildscripts (DEPRECATED): [https://github.com/theofficialgman/chromium-tegra-scripts](https://github.com/theofficialgman/chromium-tegra-scripts)

### Credits <a href="#credits" id="credits"></a>

theofficialgman (ubuntu jammy distro maintainer, L4T-Megascript)\
CTCaer (kernel/bootloader, ubuntu bionic distro maintainer, NVENC/NVDEC),\
Gavin\_Darkglider (lakka distro maintainer),\
Azkali (fedora distro maintainer),\
Ave (switchroot website host),\
DanielOgorchock(Switch controller drivers/joycond),\
Bylaws,\
Langerhans,\
Natinusala,\
stary2001 (reboot2payload),\
NVIDIA (Tegra BSP),\
cobalt2727 (L4T-Megascript),\
Everyone else in switchroot
