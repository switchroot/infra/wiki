---
description: >-
  Installation and update guide/tutorial for Switchroot L4T Ubuntu Bionic on
  Nintendo Switch
---

# L4T Ubuntu Bionic 18.04 Install Guide

**Current version: 5.1.2** (2023-8-25)\
Note: [**hekate**](https://github.com/CTCaer/hekate/releases/latest) **6.0.6 or newer MUST** be used for this release.\
Maintainer: [CTCaer](https://github.com/CTCaer) - [https://www.patreon.com/ctcaer](https://www.patreon.com/ctcaer)

If you need help you can ask in our [Discord Server](https://discord.gg/53mtKYt) under #bionic-support\
You should also take a look at our [FAQ](../faq.md) and [Common Issues](../common-issues.md) which may already answer to some of your questions

The important parts of the guide are the [**Installation**](l4t-ubuntu-bionic-installation-guide.md#installation) and [**Update**](l4t-ubuntu-bionic-installation-guide.md#updating-existing-installations-3xx5xx) sections.\
You can skip the rest if you don't want extra information about the project.

Note: The [latest LTS Release of Ubuntu (Noble 24.04) is now available](l4t-ubuntu-noble-installation-guide.md) instead of this older (Bionic 18.04) release.&#x20;

### Updating existing installations <a href="#updating-existing-installations-3xx5xx" id="updating-existing-installations-3xx5xx"></a>

If you come from a previous release and need to update.

* In L4T Ubuntu Linux, go to the `Software Updater` app and click "Install Now"\
  Or open terminal and execute: `sudo apt update && sudo apt-get dist-upgrade`.\
  After update is finished, reboot and done.
  * If Nvidia BSP was forcefully changed, creating a `.reinit` file into bootfiles directory (`sd:/switchroot/{distro}/`) will restore _Kernel_, _Kernel Modules_, _Firmware_ and other important things on the next boot and make the installation working again.\
    It can also be used if updater somehow failed to extract the modules and firmware.
* If you haven't already, go to `hekate` -> `Nyx Options` -> `Dump Joy-Con BT` and dump Joycon pairing data, with both Joycon connected to console, after first pairing them in hos.

### Prerequisites <a href="#prerequisites" id="prerequisites"></a>

* [**hekate**](https://github.com/CTCaer/hekate/releases/latest) **6.0.6 or newer**
* A 16GB SD Card **MINIMUM** (Recommended: 128GB and up U3 or U3/A2 class)
* A computer (for backing up and extracting the installation files)
* A USB stick or hdd if that method is wanted (via Dock or OTG adapter)

### Installation <a href="#installation" id="installation"></a>

1. Download the base image directly or via torrent from [here](https://download.switchroot.org/ubuntu/).
   * If eMMC or USB flashing is wanted instead of SD card, read the [USB/eMMC guide](linux-usb-or-emmc-boot.md), since a partitioning of sd card is not needed.
2. Backup your SD Card:
   * Backup normal files from FAT32 (this also backs up emuMMC if file based)
   *   If emuMMC raw partition exists, go to `hekate` -> `Tools` -> `Backup eMMC`.

       Tap on `SD emuMMC RAW Partition` button and back it up (`emuMMC BOOT0/1` and `emuMMC Raw GPP`)
   * If android exists, do a TWRP backup\
     \
     **Suggestion**: Use hekate UMS (`hekate` -> `Tools` -> `USB Tools`) with a USB-C cable connecting your switch and PC to mount your SD card and transfer files instead of swapping your card from Switch to PC.\

3.  **WARNING: This following will wipe everything on your SD card.**\
    Partition your SD card in `hekate` -> `Tools` -> `Partition SD Card` leave at least 5GB for FAT32 for installation files downloaded in step 1 **AND** recreate any partition that may have backed up.\


    <figure><img src="../.gitbook/assets/nyx20890123_204922.png" alt=""><figcaption></figcaption></figure>

    <figure><img src="../.gitbook/assets/nyx20890123_204928.png" alt=""><figcaption></figcaption></figure>
4. From your PC, extract the **7z** to the root of your SD FAT32 partition (Note that Win11's builtin 7z extract support is broken, use 7-zip and make sure not to create a new folder on extract). ALWAYS SAFELY EJECT storage when finished or you WILL corrupt the installation files. Suggestion: Use hekate UMS (`hekate` -> `Tools` -> `USB Tools`) with a USB-C cable connecting your switch and PC to mount your SD card instead of swapping your card from Switch to PC.\

5. In Hekate, go to `Tools` -> `Partition SD Card` -> `Flash Linux`.\

6.  Go to `hekate` -> `Nyx Options` -> `Dump Joy-Con BT` to dump Joycon pairing data, with both Joycon connected to console, after they were paired in hos first.\
    This must be run even if a **Switch Lite** is used, since it dumps the calibration data.\


    <figure><img src="../.gitbook/assets/nyx20890123_202109.png" alt=""><figcaption></figcaption></figure>

    <figure><img src="../.gitbook/assets/nyx20890123_204917.png" alt=""><figcaption></figcaption></figure>
7. Now you can boot L4T Ubuntu Bionic (under `More Configs`).\
   Make sure any existing online updates are done via `Software Updater` or `apt` command.\

8. After making sure everything is fine, you can now restore your backed up files
   * Copy your normal files to FAT32
   * For emuMMC use Hekate to restore your backup `hekate` -> `Tools` -> `Restore eMMC`\
     Tap on `SD emuMMC RAW Partition` button and restore it up (`emuMMC BOOT0/1` and `emuMMC Raw GPP`)
   * For Android, copy your TWRP backup to your FAT32 partition then follow the Android setup guide starting at step 4 **BUT instead of installing "Lineage zip, followed by your GApps zip" at step 5.(f on 10, g on 11) restore your backup in TWRP**

### Next Steps <a href="#next-steps" id="next-steps"></a>

* [Read about our Supported Features](linux-features.md)
  * [JoyCon Mouse/Keyboard mapping](linux-features.md#joycon-mousekeyboard-mapping)
  * [Pairing Nintendo Switch Pro Controller](linux-features.md#joycon-and-switch-pro-controller-native-support)
  * [CPU/GPU Overclocking](linux-features.md#nvidia-power-profile-applet-nvpmodel)
  * [RAM Overclocking](linux-features.md#ram-overclocking)
* [Version History / Changelog](linux-changelog.md)
* [L4T-Megascript](https://github.com/cobalt2727/L4T-Megascript/wiki/Initial-Setup#installrun-the-megascript)
  * Use the [L4T-Megascript app](https://github.com/cobalt2727/L4T-Megascript/wiki), available in your apps list (installed by default in 3.4.0+)\
    You can get or build various essential stuff, apps or emulators with a single click. All configured and optimized for Switch.

### Credits <a href="#credits" id="credits"></a>

**CTCaer** (kernel/bootloader, distro maintainer, NVENC/NVDEC)\
**Gavin\_Darkglider** (distro maintainer)\
**Azkali** (distro maintainer)\
**Ave** (repo management and host)\
**DanielOgorchock** (Switch controller drivers/joycond)\
**Bylaws**\
**Langerhans**\
**Natinusala**\
**stary2001** (reboot2payload)\
**theofficialgman** and **cobalt2727** (L4T-Megascript)\
NVIDIA (Tegra BSP)\
Everyone else in switchroot
