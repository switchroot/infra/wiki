# FAQ

<details>

<summary>Can I run steam and other x86_64 apps ?</summary>

Short Answer: No.

\
Long Answer: The Switch CPU architecture is aarch64 and unless some support is brought for other architectures by those companies to their apps you won't be able to run Steam and other x86\_64 apps natively. box64 allows for x86\_64 to arm64 emulation. box86 allows for x86 to armhf emulation but lacks proper GPU acceleration on Switch because Nvidia does not provide armhf GPU drivers.&#x20;

Steam can be installed through the [L4T-Megascript](linux/linux-features.md#general-features-supported) (check your apps list) and uses box64 and box86 (with VIRGL for extremely slow but functional x86 GPU acceleration).

Wine and Proton are compatibility layers for running Windows x86 and x86\_64 programs on x86 and x86\_64 linux. In combination with the above box86/box64, these can be used to run SOME windows x86/x86\_64 programs on linux arm64, however compatibility is VERY low, buggy, and emulation is slow. Install Wine through [pi-apps](https://pi-apps.io/install/) for a new WOW64 mode enabled Wine, other methods will NOT work or will have lower performance.

</details>

<details>

<summary>Can I emulate my legally owned game backups within Android or Linux?</summary>

Many console emulators are open sourced and/or have native support for aarch64. Projects such as RetroArch (including Lakka RetroArch Linux Distro) and RetroPie (under L4T Ubuntu Bionic) have native support. And many more.

</details>

<details>

<summary>How do I access my SWITCH SD from PC?</summary>

With your Switch connected to your PC with a USB C -> A Cable:\
In hekate\
Tools -> USB Tools -> USB Mass Storage > SD Card

Within Linux (network share):\
Setup SMB/Samba sharing on Windows or Linux (many right ways to do so)\
Mount the share in Linux on Switch (cifs-utils or gigolo)

Within Android (usb or network share):\
Download an FTP server from Google Play (requires Gapps) or plug in to a computer and select File Transfer from the USB options dropdown

</details>

<details>

<summary>Can L4T / Android run on Mariko (Switch 2019, Lite, and OLED)?</summary>

Yes! (with modchip)

Switchroot does not endorse the manufacture of or provide user support for modchips, but a chipped unit can run Switchroot Android 11, L4T Ubuntu Bionic & Jammy 5.0.0 and up, L4T Fedora, and L4T Lakka (in beta)

</details>

<details>

<summary>Can Windows run on the Switch?</summary>

Short answer: Nope! Go buy a Steam Deck.

Long answer: The Windows installer was booted as part of [a project to port edk2, an OSS UEFI bootloader, to the Switch](https://github.com/imbushuo/NintendoSwitchPkg). However, it is currently impossible to get further than the installer, as there are no Windows drivers for the Tegra SDMMC controller (needed to access storage), and writing one from scratch would be a ton of wasted effort given there are no drivers for anything else, meaning there would be no hardware accelerated graphics, no touchscreen, no JoyCons, etc., without writing drivers from scratch. Even if someone did, which they won't because it's way too much effort, it would run terribly anyway given the low-tier hardware.

</details>
