---
description: Welcome to the Switchroot Wiki
---

# Home

Switchroot is a group for open-source development on the Nintendo Switch, a Tegra X1-based game console with a FOSS bootstrap exploiting a low-level recovery bootloader. We maintain many projects for the Switch and are in the process of consolidating all our many guides into this Wiki. If you'd like to help work on one of our projects or even just edit this site, ask in the [Switchroot Official Discord Server](https://discord.gg/N9PPYXjWMY) or [Linux 4 Switch Official Discord](https://discord.gg/53mtKYt).

### Projects

* [Switchroot Android](android/)
  * [Android 15](android/android-14-15/): Upstream LineageOS 22.1 release - Tablet and Android TV for all Switch models
  * [Android 14](android/android-14-15/): Upstream LineageOS 21 release - Tablet and Android TV for all Switch models
  * [Android 11](android/android-11/): LineageOS 18.1-based release - Tablet and Android TV for all Switch models
  * [Android 10](android/android-10/): LineageOS 17.1-based release - Tablet and Android TV for Switch v1 units
  * [Android 8.1](android/android-8.1.md): \[DEPRECATED] LineageOS 15.1-based release - Tablet and Android TV for Switch v1 units
* [Linux 4 Switch Distributions](linux/linux-distributions.md): Support all Switch models
  * [L4T Ubuntu Bionic 18.04](linux/l4t-ubuntu-bionic-installation-guide.md)
  * [L4T Ubuntu Jammy 22.04](linux/l4t-ubuntu-jammy-installation-guide.md)
  * [L4T Ubuntu Noble 24.04](linux/l4t-ubuntu-noble-installation-guide.md)
  * [L4T Fedora 41](linux/l4t-fedora-installation-guide-1.md)
  * [L4T Lakka 5.0 - A RetroArch Distro](linux/l4t-lakka-install-update-guide.md)

#### Sponsorship

We are not in need of any sponsorship at this time, but special thanks to [GitBook](https://www.gitbook.com/) for sponsoring our free premium plan to host this wiki!

#### Funding

[Funding for Mariko support](https://www.gofundme.com/f/add-mariko-support-to-switchroot-projects) is complete and all currently supported releases have Mariko support! L4T Ubuntu Bionic 5.0.0 was released as of 2022/12/24, and Switchroot Android R (11) was released as of 2023/6/2, both with support for all Switch models. We thank all of those who donated to the cause.

We also accept personal donations to various contributors and Switchroot Team members:

* CTCaer (Linux & Low level developer, Hekate maintainer)\
  [https://www.patreon.com/ctcaer](https://www.patreon.com/ctcaer)
* theofficialgman (Linux & L4T-Megascript developer)\
  [https://github.com/sponsors/theofficialgman](https://github.com/sponsors/theofficialgman)
* Azkali (Linux & Low level developer)\
  [https://www.patreon.com/azkali](https://www.patreon.com/azkali)
* gavin\_darkglider (Linux & Lakka developer)\
  [https://paypal.me/gavindarkglider](https://paypal.me/gavindarkglider)
* makinbacon (Android developer)\
  [https://paypal.me/makinbacon21](https://paypal.me/makinbacon21)
* npjohnson (Android developer)\
  [https://paypal.me/nolenjohnson](https://paypal.me/nolenjohnson)
* ave (Infrastructure & Hosting)\
  [https://patreon.com/aveao](https://patreon.com/aveao)

#### Resources

* [Switchroot Discord](https://discord.gg/N9PPYXjWMY) (for Android)
* [Linux 4 Switch Discord](https://discord.gg/53mtKYt) (for Linux)
* [Switchroot Website](https://switchroot.org/)
* [Switchroot GitLab](https://gitlab.com/switchroot)
* [L4T Community GitLab](https://gitlab.com/l4t-community/)
