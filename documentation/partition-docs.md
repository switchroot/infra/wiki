# Partition Docs

As `hekate`'s partition manager purposes is to get user quickly setup, it lacks resizing capability.

To mitigate this, users can use a standard `ext4`-supported partition manager like `gparted` to manually resize, create, and destroy partitions to their hearts' content. This page is meant as a reference to understand the partition layout our projects support so anyone can jump right in and modify their configuration. Data is based off [`hekate`'s partitioner source code](https://github.com/CTCaer/hekate/blob/master/nyx/nyx_gui/frontend/gui_tools_partition_manager.c).

## Basics <a href="#basics" id="basics"></a>

It is recommended to keep a MBR or Hybrid MBR partition table to keep HOS compatibility. Generally, L4T is applied to an MBR partition unless a GPT table (within an MBR partition hence hybrid) is present, and Android must always be applied to such a GPT table. You can create your partitions in the GPT table and use Hekate's "Fix Hybrid MBR" when you are done to allow HOS to read the FAT32.

The first partition on an SD card should be FAT32 with name `hos_data` as some functions of Hekate and Android depend on this for functionality.

## Android Partitions

See your respective [android](../android/ "mention") version for partition documentation. Android partitions can be either dynamic (≥ Android 14) or legacy/System-as-Root (≤ Android 11). Note that Android requires a number of partitions either way, unlike Linux.

## Linux Partitions <a href="#linux-partitions" id="linux-partitions"></a>

L4T Linux distributions require only a single ext4 partition which can be placed anywhere in the GPT or MBR (if no GPT is present). The FS Label should be named as according to (older distros)`uenv.txt` in the `/switchroot/<distro>/` folder or (newer distros) `<distro>.ini` in the `/bootloader/ini/` folder on the FAT32 (ex: `SWR-UBU` for L4T Ubuntu Bionic). Initial rootfs is flashed via `hekate` partitioner.
