# Terminology

**Switchroot**: A group for open-source development on the Nintendo Switch, a Tegra X1-based game console

**L4S**: Linux4Switch, a subsidiary of Switchroot.

**L4T**: Linux 4 Tegra, Linux kernel for Tegra devices made by Nvidia.

**BSP**: board source package - contains necessary libraries and binaries (in this case for running linux/android on nvidia tegra devices)

**HOS**: Horizon OS, Nintendo Switch original/default firmware made by Nintendo.

[**AMS**](https://github.com/Atmosphere-NX/Atmosphere): Atmosphere, customized firmware for the Nintendo Switch.

[**Hekate**](https://github.com/CTCaer/hekate): Nintendo Switch's custom bootloader.

**Nyx**: Hekate's GUI.

#### Emoji Terminology <a href="#emoji-terminology" id="emoji-terminology"></a>

![](../.gitbook/assets/546813660178284565.webp): joke for an eternal release date on all switch projects\
![](../.gitbook/assets/819819086217216020.webp): used when user (particularly azkali) needs to sleep\
![](../.gitbook/assets/925226910408146985.webp): emoji used to show CTCaer approves this message\
![](../.gitbook/assets/927376721194418186.webp): you pinged a user who was already present... avoid doing that
