---
description: >-
  Switchroot OS's run by default on SD cards. Make sure yours is good enough to
  have the best experience!
---

# SD Card Guide

For explanations of terms used and a general lesson in the SD specs, jump to [#sd-card-terms](sd-card-guide.md#sd-card-terms "mention")

DISCLAIMER: Switchroot is neither affiliated nor associated with any SD card manufacturer, and our reviews are our opinions based on benchmarks, average binning, and other data we have collected.

## BROKEN/DEFECTIVE Cards

Silicon Power (Taishin), Teamgroup, and Foresee (Longsys)

These cards are known to have DDR200 incompatibility (corrupt data and fail to boot when used in this mode) or do not work outright so should not be used.

Additionally, any SD card that identifies as "FAKE" under Hekate -> Console info -> SD Card will NOT work.

## Recommended Cards

### Tier 1: Samsung <a href="#tier-1-samsung" id="tier-1-samsung"></a>

Highlights:

* Fastest we've seen
* Long life span
* `ddr200` support
* Good price-to-performance
* Up to 512GB

Samsung makes the best UHS-I microSD cards money can buy. The Pro Plus line (dark blue, 2022-2023) and Pro Ultimate (nearly black, 2023) are the fastest we've ever seen. These cards are compatible the the `ddr200` specification as implemented in `hekate` and the L4T kernel. This goes beyond the 104 MB/s max of standard SDR104 and allows for the best performance possible on Switchroot operating systems. The lower tier Evo and older Pro models still work very well, but some firmware bugs have been reported and they are not as fast. NOTE: errata have been observed with current Pro Ultimate cards, with some reports indicating sustained transfers may cause slowdowns. They're not bad cards by any means, and should last extremely long (plus have very long warranty), but we're not yet convinced they're worth the extra money over the Pro Plus line.

### Tier 2: SanDisk <a href="#tier-3-sandisk" id="tier-3-sandisk"></a>

Highlights:

* Moderately Fast
* Good price-to-performance
* Highly available
* Relatively cheap
* Up to 2TB

SanDisk is the brand most people think of when they think of an SD card given their wide availability, decent prices, and high market share--but they come with many caveats. The first issue is lifespan--SanDisk cards will degrade quickly over time, and this can even lead to eventual write-locks and other failures. Another big caveat with SanDisk cards is their lack of proper `ddr200` support...despite being the vendor that produced the specification originally. See the comment in [this commit](https://gitlab.com/switchroot/kernel/l4t-kernel-4.9/-/commit/92edf31455357f8d5f8bf882a747f42a49746ecf) for details. One last big SanDisk issue is branding--given their lack of year-specific branding and many SKUs, it can be hard to find a good SanDisk card. Our advice is to check for a recent listing (maybe check model number if available) and use only SanDisk Extreme cards as the Ultras and everything below are basically useless for Switchroot. High-capacity Extreme cards are often slow as well--so do note you are trading measurable performance and a good experience for space. Our real advice is to just buy a Samsung card and move on with life.

### Tier 3: Micron <a href="#tier-4-micron" id="tier-4-micron"></a>

Highlights:

* Fast
* In theory very reliable (more longevity testing needed)
* Very bad price-to-performance
* Very expensive
* Up to 1.5TB

Micron is a silicon manufacturer that sells some products, like microSD cards, directly. They produce industrial-grade cards generally meant for production devices. Likely their high cost would be less of an issue in bulk. They do sell cards in large sizes but they are prohibitively expensive, so should only be bought with that purpose in mind. That being said, they do seem to be fast, reliable cards from a reputable manufacturer.

More to come! The above is the most relevant, but we will add "reviews" for other vendors and newer models in the future.

## SD Card Terms

* HS refers to the High Speed bus for SD cards, which is no longer actually considered very fast (similar to "Fast Ethernet").
* UHS refers to the Ultra High Speed bus for SD cards, with a Roman numeral suffix for the revision. UHS-I refers to the original UHS SD specification with the standard one row of pins. UHS-II and -III achieve faster speeds with more contacts, but the Switch's microSD reader is limited to UHS-I, as most readers in such devices are.

<figure><img src=".gitbook/assets/image (3).png" alt=""><figcaption><p>Snapshot from Bus Speed chart, credit <a href="https://www.sdcard.org/developers/sd-standard-overview/bus-speed-default-speed-high-speed-uhs-sd-express/">https://www.sdcard.org/developers/sd-standard-overview/bus-speed-default-speed-high-speed-uhs-sd-express/</a></p></figcaption></figure>

* `ddr200` is essentially the SD version of the eMMC HS400 standard, created by SanDisk, to essentially allow close to double the SDR104 throughput over the UHS-I bus. However, their reference implementation required a DLL mechanism that is not supported on the Switch's reader. Every other vendor that supports the standard aligns transfers on clock edge instead of whenever based on the DLL mechanism, allowing `ddr200` to work on these cards as a version of SDR104 (normally the fastest speed spec for this reader) that samples on rising and falling edge. See the comment in [this commit](https://gitlab.com/switchroot/kernel/l4t-kernel-4.9/-/commit/92edf31455357f8d5f8bf882a747f42a49746ecf) from CTCaer for details.

<figure><img src=".gitbook/assets/image (4).png" alt=""><figcaption><p>Snapshot from "SanDisk QuickFlow" tech brief, credit <a href="https://documents.westerndigital.com/content/dam/doc-library/en_us/assets/public/western-digital/collateral/tech-brief/tech-brief-sandisk-quickflow-technology.pdf">https://documents.westerndigital.com/content/dam/doc-library/en_us/assets/public/western-digital/collateral/tech-brief/tech-brief-sandisk-quickflow-technology.pdf</a></p></figcaption></figure>

* The A rating, or Application Performance Class, is defined by the SD Association as a standard for minimum general IO performance. This is intended to differentiate cards that are better for storage operations. The A2 rating necessitates a minimum random read speed of 4000 IOPS, a minimum random write speed of 2000 IOPS, and a minimum sustained sequential write speed of 10 MB/s. All cards used for running Switchroot operating systems should be A2 (or higher if that happens).

<figure><img src=".gitbook/assets/image (1).png" alt=""><figcaption><p>Snapshot from Application Performance Class chart, credit <a href="https://www.sdcard.org/developers/sd-standard-overview/application-performance-class/">https://www.sdcard.org/developers/sd-standard-overview/application-performance-class/</a></p></figcaption></figure>

* The C, U, and V ratings, or HS Speed Class, UHS Speed Class, and Video Speed Class, are defined by the SD Association as a standard for minimum sequential write speed. This is intended to differentiate cards that are better for applications like filming video, where the relevant storage operations are generally sequential. This is not relevant for us because the bottleneck in running operating systems off of an SD card is generally random IO performance rather than sequential, so if you have a C10 U3 V30 (above V30 requires UHS-II or -III) card that is only A1, you will likely have a poor experience. Note that HS and UHS are not the same, so a U1 card must be UHS-I and be HS class 10, but a HS class 10 card does not have support UHS, and in that case would not be U1.

<figure><img src=".gitbook/assets/image.png" alt=""><figcaption><p>Snapshot from Speed Class chart, credit <a href="https://www.sdcard.org/developers/sd-standard-overview/speed-class/">https://www.sdcard.org/developers/sd-standard-overview/speed-class/</a></p></figcaption></figure>
